#ifndef MULTIPROCESS_H
#define MULTIPROCESS_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <vector>

struct WorkItem {
   std::shared_ptr<Lookahead> lookahead;
   std::deque<std::shared_ptr<PossibilityPair>> possibilities;
};

extern std::queue<WorkItem> work_queue;
extern std::mutex work_queue_mutex;
extern std::condition_variable work_queue_cv;

void worker_thread();
void expand_parallel(std::shared_ptr<Lookahead>& lookahead);

#endif