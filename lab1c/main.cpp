#include "crow_all.h"
#include <sqlite3.h>
#include <iostream>
#include <regex>
#include <bcrypt/BCrypt.hpp>
#include <random>
#include <sstream>
#include <string>

#include "task31.h"
#include "db_service.h"

sqlite3* db;

bool validate_board_string(const std::string& board_str) {
    std::string board = board_str;
    board.erase(std::remove(board.begin(), board.end(), '\\'), board.end());

    std::regex re(R"(^([.WB]*\n)+[.WB]*$)");
    if (!std::regex_match(board, re)) {
        std::cout << "-- Wrong format!" << std::endl;
        return false;
    }

    std::istringstream stream(board);
    std::string line;
    std::vector<std::string> lines;

    while (std::getline(stream, line)) {
        lines.push_back(line);
    }

    size_t first_line_length = lines[0].length();
    if (std::any_of(lines.begin(), lines.end(), [first_line_length](const std::string& l) {
        return l.length() != first_line_length;
    })) {
        std::cout << "-- Lines not equal!" << std::endl;
        return false;
    }

    if (lines.size() < 2 || first_line_length < 2) {
        std::cout << "-- <2 lines or symbols" << std::endl;
        return false;
    }

    return true;
}

std::string solve_masyu(const std::string& board_str) {
    auto board = board_from_string(board_str);
    std::cout << print_big_board(board);
    try {
        auto solved_board = solve(board);
        if (!solved_board.empty()) {
            return print_big_board(solved_board);
        } else {
            throw std::runtime_error("Unable to solve the puzzle");
        }
    } catch (const std::exception& e) {
        throw crow::exception(e.what());
    }
}

std::string generate_correct_problem() {
    std::vector<std::string> board_str = {
        ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B",
        ".....B\n..B...\n....WW\n......\n...B..\nB...W.",
        "....W.\nW.....\n.....B\nB.....\n.....W\n......",
        "....W.\nW.....\n.B....\n..B..W\n......\n.W...B",
        ".W..WB\n......\n.W.W..\n....B.\n.....W\n..W..."
    };

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, board_str.size() - 1);

    return board_str[dis(gen)];
}

int main() {
    crow::SimpleApp app;

    // Connect to SQLite database
    if (sqlite3_open("database.db", &db) != SQLITE_OK) {
        std::cerr << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return EXIT_FAILURE;
    }

    CROW_ROUTE(app, "/solve").methods("POST"_method)
    ([](const crow::request& req) {
        auto body = crow::json::load(req.body);
        std::string problem_str = body["problem_str"].s();
        problem_str.erase(std::remove(problem_str.begin(), problem_str.end(), '\\'), problem_str.end());

        if (validate_board_string(problem_str)) {
            // Insert problem into database
            db_insert_problem(db, problem_str);

            std::string solving = solve_masyu(problem_str);

            // Insert solving to the existing problem
            db_insert_solving_to_existing_problem(db, problem_str, solving);

            return crow::response(200, crow::json::wvalue({{"solving", solving}}));
        }
        return crow::response(400, "Invalid input data. Only . W B \\n symbols are allowed. >2 in each line, >2 lines. Lines are equal length.");
    });

    CROW_ROUTE(app, "/generate")
    ([] {
        auto problem = db_select_random_problem(db);
        auto problem_board = print_big_board(board_from_string(problem));
        return crow::response(crow::json::wvalue({{"problem", problem}, {"problemboard", problem_board}}));
    });

    CROW_ROUTE(app, "/getproblem")
    ([](const crow::request& req) {
        auto query_params = req.url_params;
        int problem_id = std::stoi(query_params.get("problem_id"));
        auto problem = db_select_problem_by_id(db, problem_id);
        auto problem_board = print_big_board(board_from_string(problem));
        return crow::response(crow::json::wvalue({{"problem", problem}, {"problemboard", problem_board}}));
    });

    CROW_ROUTE(app, "/addproblem").methods("POST"_method)
    ([](const crow::request& req) {
        auto body = crow::json::load(req.body);
        std::string problem_str = body["problem_str"].s();
        problem_str.erase(std::remove(problem_str.begin(), problem_str.end(), '\\'), problem_str.end());

        if (validate_board_string(problem_str)) {
            db_insert_problem(db, problem_str);
            return crow::response(crow::json::wvalue({{"problem", problem_str}}));
        }
        return crow::response(400, "Invalid input data. Only . W B \\n symbols are allowed. >2 in each line, >2 lines. Lines are equal length.");
    });

    CROW_ROUTE(app, "/register").methods("POST"_method)
    ([](const crow::request& req) {
        auto body = crow::json::load(req.body);
        std::string username = body["username"].s();
        std::string password_str = body["password_str"].s();

        if (db_check_user_exists(db, username)) {
            return crow::response(400, "Користувач з таким ім'ям вже існує");
        }

        std::string hashed_password = BCrypt::generateHash(password_str);
        std::string developer_key = generate_developer_key();

        if (db_add_user(db, username, hashed_password, developer_key)) {
            return crow::response(crow::json::wvalue({{"message", "Реєстрація успішна"}, {"developer_key", developer_key}}));
        }
        return crow::response(400, "Помилка реєстрації");
    });

    CROW_ROUTE(app, "/login").methods("POST"_method)
    ([](const crow::request& req) {
        auto body = crow::json::load(req.body);
        std::string username = body["username"].s();
        std::string password_str = body["password_str"].s();
        std::string developer_key = body["developer_key"].s();

        if (!developer_key.empty()) {
            if (db_check_user_exists(db, username)) {
                auto user_db = db_get_user_by_username(db, username);
                if (developer_key == user_db["developer_key"]) {
                    return crow::response(crow::json::wvalue({{"message", "Успішний вхід за допомогою девелоперського ключа"}}));
                }
            }
        } else {
            if (db_check_user_exists(db, username)) {
                auto user_db = db_get_user_by_username(db, username);
                if (BCrypt::validatePassword(password_str, user_db["hashed_password"])) {
                    return crow::response(crow::json::wvalue({{"message", "Успішний вхід за допомогою пароля"}}));
                }
            }
        }
        return crow::response(401, "Неправильне ім'я користувача або пароль або ключ");
    });

    app.port(8080).multithreaded().run();

    sqlite3_close(db);
    return 0;
}
