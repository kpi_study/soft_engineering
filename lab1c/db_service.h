#ifndef DB_SERVICE_H
#define DB_SERVICE_H

#include <mysql_driver.h>
#include <mysql_connection.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <cppconn/exception.h>
#include <string>
#include <vector>

sql::Connection* get_connection();
bool insert_problem(sql::Connection* connection, const std::string& problem_str);
std::string select_problem_by_id(sql::Connection* connection, int problem_id);
std::string select_random_problem(sql::Connection* connection);
bool insert_solving_to_existing_problem_id(sql::Connection* connection, int problem_id, const std::string& solving_str);
bool insert_solving_to_existing_problem(sql::Connection* connection, const std::string& problem, const std::string& solving_str);
std::vector<std::string> select_solving_by_existing_problem_id(sql::Connection* connection, int problem_id);
std::vector<std::string> select_solving_by_existing_problem(sql::Connection* connection, const std::string& problem);
bool add_user(sql::Connection* connection, const std::string& username, const std::string& password_str, const std::string& developer_key);
bool check_user_exists(sql::Connection* connection, const std::string& username);
std::vector<std::map<std::string, std::string>> get_all_users(sql::Connection* connection);
std::map<std::string, std::string> get_user_by_username(sql::Connection* connection, const std::string& username);

#endif
