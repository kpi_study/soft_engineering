#ifndef TASK31_H
#define TASK31_H

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <deque>
#include <memory>

enum class Direction {
   UP,
   DOWN,
   LEFT,
   RIGHT
};

Direction opposite(Direction direction);

struct LoopException : public std::runtime_error {
   LoopException(const std::unordered_set<std::pair<int, int>>& loop);
   void validate_solved(const std::unordered_map<std::pair<int, int>, char>& circles) const;
   std::unordered_set<std::pair<int, int>> loop;
};

struct SolvedException : public std::runtime_error {
   SolvedException(const Board& board);
   Board board;
};

struct CellLine {
   std::set<Direction> is_set;
   std::set<Direction> cannot_set;

   CellLine();
   CellLine(const std::set<Direction>& is_set, const std::set<Direction>& cannot_set);

   CellLine set_direction(Direction direction) const;
   CellLine disallow_direction(Direction direction) const;
   CellLine get_through() const;
   CellLine get_bent() const;
   bool is_done() const;
   std::set<Direction> could_set() const;
   std::optional<Direction> other_out(Direction direction) const;
};

struct LineSegment {
   std::pair<int, int> start;
   Direction start_direction;
   std::pair<int, int> end;
   Direction end_direction;
   std::set<std::pair<int, int>> contains;

   std::pair<int, int> other_end(const std::pair<int, int>& coord) const;
   LineSegment get() const;
};

std::deque<LineSegment> extend_line_segments(const std::deque<LineSegment>& line_segments, const std::unordered_map<std::pair<int, int>, CellLine>& cell_lines);
std::deque<LineSegment> discover_line_segments(const std::unordered_map<std::pair<int, int>, CellLine>& cell_lines, const std::unordered_set<std::pair<int, int>>& seen = {});
std::deque<std::pair<std::pair<int, int>, Direction>> loop_path(const std::pair<int, int>& coord, Direction direction, const std::unordered_map<std::pair<int, int>, CellLine>& cell_lines);
std::pair<int, int> direction_move(int x, int y, Direction direction);

class Board {
public:
   Board(int width, int height, const std::unordered_map<std::pair<int, int>, char>& circles);
   Board(int w, int h, const std::map<std::pair<int, int>, bool>& c, const std::map<std::pair<int, int>, CellLine>& cl, const std::vector<LineSegment>& ls);

   std::map<std::pair<int, int>, CellLine> default_cell_lines() const;
   std::vector<LineSegment> default_line_segments() const;
   Board evolve(const std::map<std::pair<int, int>, CellLine>& new_cell_lines) const;
   std::set<Direction> edges_at(int x, int y) const;
   Board set_direction(int x, int y, const Direction& direction) const;
   Board disallow_direction(int x, int y, const Direction& direction) const;
   Board set_through(int x, int y) const;
   Board set_bent(int x, int y) const;
   Board propagate_change(const std::map<std::pair<int, int>, CellLine>& changes) const;
   std::string to_string() const;

   int width;
   int height;
   std::map<std::pair<int, int>, bool> circles;
   std::map<std::pair<int, int>, CellLine> cell_lines;
   std::vector<LineSegment> line_segments;

   friend std::ostream& operator<<(std::ostream& os, const Board& board);
};

class Ref {
public:
   Ref(const std::shared_ptr<Lookahead>& value);
   template <typename T>
   T& operator->*(T Lookahead::*member) const;

private:
   std::shared_ptr<Lookahead> ref;
};

class Lookahead {
public:
   Lookahead(const Board& board, const std::deque<std::shared_ptr<PossibilityPair>>& possibilities, const std::shared_ptr<Lookahead>& parent);
   static std::shared_ptr<Lookahead> new_lookahead(const Board& board, const std::shared_ptr<Lookahead>& parent = nullptr);
   std::shared_ptr<Lookahead> get_sibling() const;

   std::deque<std::shared_ptr<PossibilityPair>> possibilities;
   std::weak_ptr<Lookahead> parent;
   Board board;
};

void explore(std::shared_ptr<Lookahead>& lookahead);
void expand(std::shared_ptr<Lookahead>& lookahead);
std::deque<std::shared_ptr<PossibilityPair>> get_possibility_list(const std::shared_ptr<Lookahead>& lookahead);

class PossibilityPair {
public:
   PossibilityPair(const std::shared_ptr<Lookahead>& yes, const std::shared_ptr<Lookahead>& no, const std::shared_ptr<Lookahe