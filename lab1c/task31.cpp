#include <iostream>
#include <vector>
#include <unordered_map>
#include <set>
#include <stdexcept>
#include <algorithm>
#include <memory>
#include <deque>
#include <functional>
#include <cassert>
#include <sstream>
#include <unordered_set>

enum class Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};

Direction opposite(Direction direction) {
    switch (direction) {
        case Direction::UP:
            return Direction::DOWN;
        case Direction::DOWN:
            return Direction::UP;
        case Direction::LEFT:
            return Direction::RIGHT;
        case Direction::RIGHT:
            return Direction::LEFT;
    }
}

struct LoopException : public std::runtime_error {
    LoopException(const std::unordered_set<std::pair<int, int>>& loop)
        : std::runtime_error("Loop detected"), loop(loop) {}

    void validate_solved(const std::unordered_map<std::pair<int, int>, char>& circles) const {
        for (const auto& coord : loop) {
            if (circles.at(coord) != 'B') {
                throw *this;
            }
        }
    }

    std::unordered_set<std::pair<int, int>> loop;
};

struct SolvedException : public std::runtime_error {
    SolvedException(const Board& board)
        : std::runtime_error("Board solved"), board(board) {}

    Board board;
};

struct CellLine {
    std::set<Direction> is_set;
    std::set<Direction> cannot_set;

    CellLine() = default;

    CellLine(const std::set<Direction>& is_set, const std::set<Direction>& cannot_set)
        : is_set(is_set), cannot_set(cannot_set) {}

    CellLine set_direction(Direction direction) const {
        if (is_set.count(direction)) {
            return *this;
        }
        if (cannot_set.count(direction)) {
            throw std::invalid_argument("Can't set direction on cell");
        }

        std::set<Direction> new_is_set = is_set;
        new_is_set.insert(direction);
        std::set<Direction> new_cannot_set = cannot_set;
        if (new_is_set.size() == 2) {
            new_cannot_set = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
            for (const auto& dir : new_is_set) {
                new_cannot_set.erase(dir);
            }
        } else if (new_cannot_set.size() == 2) {
            new_is_set = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
            for (const auto& dir : new_cannot_set) {
                new_is_set.erase(dir);
            }
        }
        return CellLine(new_is_set, new_cannot_set);
    }

    CellLine disallow_direction(Direction direction) const {
        if (cannot_set.count(direction)) {
            return *this;
        }
        if (is_set.count(direction)) {
            throw std::invalid_argument("Can't disallow direction on cell");
        }

        std::set<Direction> new_cannot_set = cannot_set;
        new_cannot_set.insert(direction);
        std::set<Direction> new_is_set = is_set;
        if (new_cannot_set.size() == 2 && new_is_set.size() == 1) {
            new_is_set = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
            for (const auto& dir : new_cannot_set) {
                new_is_set.erase(dir);
            }
        } else if (new_cannot_set.size() == 3) {
            new_cannot_set = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
        }
        return CellLine(new_is_set, new_cannot_set);
    }

    CellLine get_through() const {
        int num_set = is_set.size();
        if (num_set == 2) {
            auto it = is_set.begin();
            Direction one = *it;
            Direction other = *(++it);
            if (opposite(one) != other) {
                throw std::invalid_argument("Cell is already bent");
            }
            return *this;
        }
        if (num_set == 1) {
            Direction one = *is_set.begin();
            return set_direction(opposite(one));
        }
        if (num_set != 0) {
            throw std::invalid_argument("Expected none set");
        }

        int num_cannot_set = cannot_set.size();
        if (num_cannot_set == 1) {
            Direction one = *cannot_set.begin();
            std::set<Direction> cannot_set = {one, opposite(one)};
            std::set<Direction> is_set = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
            for (const auto& dir : cannot_set) {
                is_set.erase(dir);
            }
            return CellLine(is_set, cannot_set);
        }
        if (num_cannot_set == 2) {
            std::set<Direction> is_set = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
            for (const auto& dir : cannot_set) {
                is_set.erase(dir);
            }
            auto it = is_set.begin();
            Direction one = *it;
            Direction other = *(++it);
            if (opposite(one) != other) {
                throw std::invalid_argument("No straight path exists through cell");
            }
            return CellLine(is_set, cannot_set);
        }
        if (num_cannot_set == 4) {
            throw std::invalid_argument("Cell must be blank");
        }
        if (num_cannot_set != 0) {
            throw std::invalid_argument("Expected no cannot_set");
        }

        return *this;
    }

    CellLine get_bent() const {
        int num_set = is_set.size();
        if (num_set == 2) {
            auto it = is_set.begin();
            Direction one = *it;
            Direction other = *(++it);
            if (opposite(one) == other) {
                throw std::invalid_argument("Cell is already straight-through");
            }
            return *this;
        }
        if (num_set == 1) {
            Direction one = *is_set.begin();
            return disallow_direction(opposite(one));
        }

        int num_cannot_set = cannot_set.size();
        if (num_cannot_set == 1) {
            Direction one = *cannot_set.begin();
            return set_direction(opposite(one));
        }
        if (num_cannot_set == 2) {
            std::set<Direction> is_set = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
            for (const auto& dir : cannot_set) {
                is_set.erase(dir);
            }
            auto it = is_set.begin();
            Direction one = *it;
            Direction other = *(++it);
            if (opposite(one) == other) {
                throw std::invalid_argument("No bent path exists through cell");
            }
            return CellLine(is_set, cannot_set);
        }
        if (num_cannot_set == 4) {
            throw std::invalid_argument("Cell must be blank");
        }
        if (num_cannot_set != 0) {
            throw std::invalid_argument("Expected no cannot_set");
        }

        return *this;
    }

    bool is_done() const {
        return is_set.size() + cannot_set.size() == 4;
    }

    std::set<Direction> could_set() const {
        std::set<Direction> all_directions = {Direction::UP, Direction::DOWN, Direction::LEFT, Direction::RIGHT};
        std::set<Direction> could_set;
        for (const auto& dir : all_directions) {
            if (!is_set.count(dir) && !cannot_set.count(dir)) {
                could_set.insert(dir);
            }
        }
        return could_set;
    }

    std::optional<Direction> other_out(Direction direction) const {
        std::set<Direction> other = is_set;
        other.erase(direction);
        if (other.empty()) {
            return std::nullopt;
        }
        return *other.begin();
    }
};

struct LineSegment {
    std::pair<int, int> start;
    Direction start_direction;
    std::pair<int, int> end;
    Direction end_direction;
    std::set<std::pair<int, int>> contains;

    std::pair<int, int> other_end(const std::pair<int, int>& coord) const {
        if (coord == start) {
            return {end.first, end.second};
        }
        return {start.first, start.second};
    }

    LineSegment get() const {
        return *this;
    }
};

std::deque<LineSegment> extend_line_segments(const std::deque<LineSegment>& line_segments, const std::unordered_map<std::pair<int, int>, CellLine>& cell_lines) {
    std::unordered_map<std::pair<int, int>, LineSegment> coord_lookup;
    for (const auto& seg : line_segments) {
        coord_lookup[seg.start] = seg;
        coord_lookup[seg.end] = seg;
    }
    std::unordered_set<LineSegment> seen_segs;
    std::deque<LineSegment> new_segs;
    for (const auto& segment : line_segments) {
        if (seen_segs.count(segment)) {
            continue;
        }
        seen_segs.insert(segment);

        std::unordered_set<std::pair<int, int>> loop = segment.contains;
        std::unordered_map<std::pair<int, int>, Direction> changed_ends;

        std::vector<std::tuple<std::string, std::pair<int, int>, Direction>> segment_ends = {
            {"start", segment.start, segment.start_direction},
            {"end", segment.end, segment.end_direction},
        };
        for (const auto& [side, coord, segment_direction] : segment_ends) {
            const auto& cell = cell_lines.at(coord);
            if (cell.is_set.size() == 1) {
                continue;
            }
            auto next_dir = cell.other_out(segment_direction);
            std::deque<std::pair<std::pair<int, int>, Direction>> iterator;
            for (const auto& [coord, next_dir] : iterator) {
                if (coord_lookup.count(coord)) {
                    auto merge_seg = coord_lookup.at(coord);
                    if (merge_seg == segment) {
                        throw LoopException(loop);
                    }
                    seen_segs.insert(merge_seg);
                    std::tie(coord, next_dir) = merge_seg.other_end(coord);
                    auto move_dir = cell_lines.at(coord).other_out(next_dir);
                    iterator.swap(loop_path(coord, move_dir, cell_lines));
                } else {
                    loop.insert(coord);
                }
            }
            changed_ends[side] = coord;
            changed_ends[side + "_direction"] = next_dir;
        }

        new_segs.push_back({
            segment.start,
            segment.start_direction,
            segment.end,
            segment.end_direction,
            loop,
        });
    }
    return new_segs;
}

std::deque<LineSegment> discover_line_segments(const std::unordered_map<std::pair<int, int>, CellLine>& cell_lines, const std::unordered_set<std::pair<int, int>>& seen = {}) {
    std::unordered_set<std::pair<int, int>> seen_set = seen;
    std::deque<LineSegment> line_segments;
    for (const auto& [coord, cell] : cell_lines) {
        if (seen_set.count(coord) || !cell.is_set) {
            continue;
        }

        std::unordered_set<std::pair<int, int>> loop = {coord};
        std::pair<int, int> start = coord;
        if (cell.is_set.size() == 1) {
            Direction forward_dir = *cell.is_set.begin();
            Direction back_dir = forward_dir;
            for (std::pair<int, int> coord = start; true; std::tie(coord, back_dir) = loop_path(coord, back_dir, cell_lines)) {
                if (loop.count(coord)) {
                    throw LoopException(loop);
                }
                loop.insert(coord);
                if (coord == start && back_dir == forward_dir) {
                    break;
                }
            }
        } else {
            Direction forward_dir = *cell.is_set.begin();
            Direction back_dir = *std::next(cell.is_set.begin());
            for (std::pair<int, int> coord = start; true; std::tie(coord, forward_dir) = loop_path(coord, forward_dir, cell_lines)) {
                loop.insert(coord);
                if (coord == start) {
                    break;
                }
            }
            for (std::pair<int, int> coord = start; true; std::tie(coord, back_dir) = loop_path(coord, back_dir, cell_lines)) {
                if (loop.count(coord)) {
                    throw LoopException(loop);
                }
                loop.insert(coord);
                if (coord == start && back_dir == forward_dir) {
                    break;
                }
            }
        }

        seen_set.insert(loop.begin(), loop.end());
        line_segments.push_back({
            start,
            opposite(cell.is_set.begin()->first),
            *loop.rbegin(),
            opposite(cell.is_set.begin()->first),
            loop,
        });
    }
    return line_segments;
}

std::deque<std::pair<std::pair<int, int>, Direction>> loop_path(const std::pair<int, int>& coord, Direction direction, const std::unordered_map<std::pair<int, int>, CellLine>& cell_lines) {
    std::pair<int, int> xy = coord;
    while (direction != Direction{}) {
        std::tie(xy.first, xy.second) = direction_move(xy.first, xy.second, direction);
        direction = opposite(direction);
        co_yield {xy, direction};
        direction = cell_lines.at(xy).other_out(direction).value();
    }
}

std::pair<int, int> direction_move(int x, int y, Direction direction) {
    switch (direction) {
        case Direction::UP:
            return {x, y - 1};
        case Direction::DOWN:
            return {x, y + 1};
        case Direction::LEFT:
            return {x - 1, y};
        case Direction::RIGHT:
            return {x + 1, y};
    }
}

std::deque<LineSegment> extend_line_segments(const std::deque<LineSegment>& line_segments, const std::unordered_map<std::pair<int, int>, CellLine>& cell_lines);

Board::Board(int width, int height, const std::unordered_map<std::pair<int, int>, char>& circles)
    : width(width), height(height), circles(circles), cell_lines(), line_segments() {
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            cell_lines[{x, y}] = CellLine({}, edges_at(x, y));
        }
    }
    line_segments = discover_line_segments(cell_lines);
}

class Board {
public:
    int width;
    int height;
    std::map<std::pair<int, int>, bool> circles;
    std::map<std::pair<int, int>, CellLine> cell_lines;
    std::vector<LineSegment> line_segments;

    Board(int w, int h, const std::map<std::pair<int, int>, bool>& c, 
          const std::map<std::pair<int, int>, CellLine>& cl, 
          const std::vector<LineSegment>& ls)
        : width(w), height(h), circles(c), cell_lines(cl), line_segments(ls) {}

    std::map<std::pair<int, int>, CellLine> default_cell_lines() const {
        std::map<std::pair<int, int>, CellLine> default_lines;
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                default_lines[{x, y}] = CellLine{std::set<Direction>(), edges_at(x, y)};
            }
        }
        return default_lines;
    }

    std::vector<LineSegment> default_line_segments() const {
        return discover_line_segments(cell_lines);
    }

    Board evolve(const std::map<std::pair<int, int>, CellLine>& new_cell_lines) const {
        try {
            auto line_segments = extend_line_segments(this->line_segments, new_cell_lines);
            std::set<std::pair<int, int>> seen;
            for (const auto& seg : line_segments) {
                seen.insert(seg.contains.begin(), seg.contains.end());
            }
            auto new_segments = discover_line_segments(new_cell_lines, seen);
            line_segments.insert(line_segments.end(), new_segments.begin(), new_segments.end());
        } catch (const LoopException& exc) {
            exc.validate_solved(circles);
            throw SolvedException(Board(width, height, circles, new_cell_lines, {}));
        }

        return Board(width, height, circles, new_cell_lines, line_segments);
    }

    std::set<Direction> edges_at(int x, int y) const {
        std::set<Direction> edges;
        if (x == 0) edges.insert(Direction::LEFT);
        if (y == 0) edges.insert(Direction::UP);
        if (y == height - 1) edges.insert(Direction::DOWN);
        if (x == width - 1) edges.insert(Direction::RIGHT);
        return edges;
    }

    Board set_direction(int x, int y, const Direction& direction) const {
        auto old_cell = cell_lines.at({x, y});
        auto new_cell = old_cell.set_direction(direction);
        if (new_cell == old_cell) return *this;
        return propagate_change({{{x, y}, new_cell}});
    }

    Board disallow_direction(int x, int y, const Direction& direction) const {
        auto old_cell = cell_lines.at({x, y});
        auto new_cell = old_cell.disallow_direction(direction);
        if (new_cell == old_cell) return *this;
        return propagate_change({{{x, y}, new_cell}});
    }

    Board set_through(int x, int y) const {
        auto old_cell = cell_lines.at({x, y});
        auto new_cell = old_cell.get_through();
        if (new_cell == old_cell) return *this;
        return propagate_change({{{x, y}, new_cell}});
    }

    Board set_bent(int x, int y) const {
        auto old_cell = cell_lines.at({x, y});
        auto new_cell = old_cell.get_bent();
        if (new_cell == old_cell) return *this;
        return propagate_change({{{x, y}, new_cell}});
    }

    Board propagate_change(const std::map<std::pair<int, int>, CellLine>& changes) const {
        std::map<std::pair<int, int>, CellLine> cell_lookup = cell_lines;
        cell_lookup.insert(changes.begin(), changes.end());

        std::deque<std::pair<int, int>> positions;
        for (const auto& change : changes) {
            positions.push_back(change.first);
        }

        while (!positions.empty()) {
            auto [x, y] = positions.front();
            positions.pop_front();
            for (const auto& direction : changes.at({x, y}).is_set) {
                auto [mx, my] = direction.move(x, y);
                auto old_cell = cell_lookup.at({mx, my});
                auto new_cell = old_cell.set_direction(direction.opposite());
                if (new_cell == old_cell) continue;
                positions.push_back({mx, my});
                changes[{mx, my}] = new_cell;
            }

            for (const auto& direction : changes.at({x, y}).cannot_set) {
                auto [mx, my] = direction.move(x, y);
                auto it = cell_lookup.find({mx, my});
                if (it == cell_lookup.end()) continue;
                auto old_cell = it->second;
                auto new_cell = old_cell.disallow_direction(direction.opposite());
                if (new_cell == old_cell) continue;
                positions.push_back({mx, my});
                changes[{mx, my}] = new_cell;
            }
        }
        return evolve(cell_lookup);
    }

    friend std::ostream& operator<<(std::ostream& os, const Board& board) {
        os << "Board";
        return os;
    }
};

std::string Board::to_string() const {
    std::ostringstream oss;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            oss << (circles.count({x, y}) ? circles.at({x, y}) : '.');
        }
        oss << '\n';
    }
    return oss.str();
}

std::deque<std::pair<std::pair<int, int>, Direction>> get_possibility_list(const Lookahead& lookahead);

class Ref {
public:
    Ref(const std::shared_ptr<Lookahead>& value) : ref(value) {}

    template <typename T>
    T& operator->*(T Lookahead::*member) const {
        return (ref.get()->*member);
    }

private:
    std::shared_ptr<Lookahead> ref;
};

class Lookahead {
public:
    Lookahead(const Board& board, const std::deque<std::shared_ptr<PossibilityPair>>& possibilities, const std::shared_ptr<Lookahead>& parent)
        : board(board), possibilities(possibilities), parent(parent) {}

    static std::shared_ptr<Lookahead> new_lookahead(const Board& board, const std::shared_ptr<Lookahead>& parent = nullptr) {
        return std::make_shared<Lookahead>(board, UNEXPLORED, parent);
    }

    std::shared_ptr<Lookahead> get_sibling() const {
        auto pos = parent.lock();
        assert(pos);
        assert(*pos->yes == *this || *pos->no == *this);
        return *pos->yes == *this ? pos->no : pos->yes;
    }

    std::deque<std::shared_ptr<PossibilityPair>> possibilities;
    std::weak_ptr<Lookahead> parent;
    Board board;
};

void explore(std::shared_ptr<Lookahead>& lookahead);

void expand(std::shared_ptr<Lookahead>& lookahead);

std::deque<std::shared_ptr<PossibilityPair>> get_possibility_list(const std::shared_ptr<Lookahead>& lookahead);

class PossibilityPair {
public:
    PossibilityPair(const std::shared_ptr<Lookahead>& yes, const std::shared_ptr<Lookahead>& no, const std::shared_ptr<Lookahead>& parent)
        : yes(yes), no(no), parent(parent) {}

    static std::shared_ptr<PossibilityPair> new_possibility_pair(const std::shared_ptr<Lookahead>& yes, const std::shared_ptr<Lookahead>& no, const std::shared_ptr<Lookahead>& parent) {
        return std::make_shared<PossibilityPair>(yes, no, parent);
    }

    std::shared_ptr<Lookahead> yes;
    std::shared_ptr<Lookahead> no;
    std::weak_ptr<Lookahead> parent;
};

void solve_known_constraints(Board& board);

std::deque<std::shared_ptr<PossibilityPair>> get_possibility_list(const std::shared_ptr<Lookahead>& lookahead) {
    std::deque<std::shared_ptr<PossibilityPair>> possibilities;
    std::unordered_set<Direction> mask = {Direction::RIGHT, Direction::DOWN};
    const Board& board = lookahead->board;
    for (const auto& [coord, cell] : board.cell_lines) {
        for (const auto& direction : cell.could_set()) {
            try {
                possibilities.push_back(std::make_shared<PossibilityPair>(solve_known_constraints(board.set_direction(coord.first, coord.second, direction)), board, lookahead));
            } catch (const std::invalid_argument&) {}
            try {
                possibilities.push_back(std::make_shared<PossibilityPair>(solve_known_constraints(board.disallow_direction(coord.first, coord.second, direction)), board, lookahead));
            } catch (const std::invalid_argument&) {}
        }
    }
    return possibilities;
}

void explore(std::shared_ptr<Lookahead>& lookahead) {
    std::deque<std::shared_ptr<Lookahead>> queue = {lookahead};
    while (!queue.empty()) {
        std::shared_ptr<Lookahead> lookahead = queue.front();
        queue.pop_front();
        if (lookahead->possibilities == UNEXPLORED) {
            expand(lookahead);
            return;
        }
        for (const auto& pos : lookahead->possibilities) {
            queue.push_back(pos->yes);
            queue.push_back(pos->no);
        }
    }
}

void expand(std::shared_ptr<Lookahead>& lookahead) {
    std::deque<std::shared_ptr<PossibilityPair>> possibilities = get_possibility_list(lookahead);
    if (possibilities.empty()) {
        if (!lookahead->parent.expired()) {
            std::shared_ptr<Lookahead> sibling = lookahead->get_sibling();
            std::shared_ptr<Lookahead> parent = lookahead->parent.lock();
            assert(parent);
            sibling->parent = parent->parent;
            parent = sibling;
            if (sibling->possibilities != UNEXPLORED) {
                for (const auto& pos : sibling->possibilities) {
                    pos->parent = parent;
                }
            }
        }
    } else if (possibilities.size() == 1) {
        lookahead->possibilities = possibilities;
    } else {
        lookahead->board = *possibilities.front()->yes;
    }
}

void solve(Board& board) {
    try {
        std::shared_ptr<Lookahead> root = Lookahead::new_lookahead(solve_known_constraints(board));
        Board last_seen_board = root->board;
        while (explore(root)) {
            if (last_seen_board != root->board) {
                std::cout << root->board.to_string() << std::endl;
                last_seen_board = root->board;
            }
        }
    } catch (const SolvedException& exc) {
        return exc.board;
    }
    return nullptr;
}

Board board_from_string(const std::string& input_str) {
    std::istringstream iss(input_str);
    std::vector<std::string> board_lines;
    std::string line;
    while (std::getline(iss, line)) {
        board_lines.push_back(line);
    }
    std::unordered_map<std::pair<int, int>, char> circles;
    for (int y = 0; y < board_lines.size(); ++y) {
        for (int x = 0; x < board_lines[y].size(); ++x) {
            if (board_lines[y][x] != '.') {
                circles[{x, y}] = board_lines[y][x];
            }
        }
    }
    return Board(board_lines[0].size(), board_lines.size(), circles);
}


const std::map<std::set<Direction>, char> INNER_CELL_LINE = {
    {{Direction::DOWN, Direction::UP}, '│'},
    {{Direction::LEFT, Direction::RIGHT}, '─'},
    {{Direction::LEFT, Direction::DOWN}, '┐'},
    {{Direction::LEFT, Direction::UP}, '┘'},
    {{Direction::RIGHT, Direction::UP}, '└'},
    {{Direction::RIGHT, Direction::DOWN}, '┌'},
};

std::string print_big_board(const std::vector<std::vector<char>>& board, const std::map<std::pair<int, int>, std::set<Direction>>& cell_lines, const std::set<std::pair<int, int>>& circles) {
    std::ostringstream board_lines;
    int width = board[0].size();
    int height = board.size();

    board_lines << "┌" << std::string(width, '─') << "┐\n";

    for (int row = 0; row < height; ++row) {
        board_lines << "│";
        for (int col = 0; col < width; ++col) {
            auto cell = cell_lines.at({col, row});
            if (circles.find({col, row}) != circles.end()) {
                board_lines << board[row][col];
            } else {
                board_lines << INNER_CELL_LINE.at(cell);
            }
            if (cell.find(Direction::RIGHT) != cell.end()) {
                board_lines << "─";
            } else {
                board_lines << "│";
            }
        }
        board_lines << "\n";
    }

    board_lines << "└" << std::string(width, '─') << "┘\n";
    return board_lines.str();
}

int main() {
    std::string board_str = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B\n";
    auto board = board_from_string(board_str);
    std::cout << print_big_board(board) << std::endl;
    auto solved_board = solve(board);
    if (solved_board) {
        std::cout << print_big_board(solved_board) << std::endl;
    }
    return 0;
}