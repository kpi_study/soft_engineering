#include <gtest/gtest.h>
#include "main.cpp"

TEST(Masyu, ValidateBoardString) {
    ASSERT_TRUE(validate_board_string(".W....\n...B..\nW.W...\n.....W\n..B...\n.....B"));
    ASSERT_FALSE(validate_board_string("Invalid\nBoard\nString"));
    ASSERT_FALSE(validate_board_string("\n\n"));
    ASSERT_FALSE(validate_board_string(".W\n..."));
}

TEST(Masyu, SolveMasyu) {
    std::string input_board = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B";
    std::string expected_solving = "⬆️🔵⬆️🔘🔘🔘\n🔘🔘🔘🔵🔘🔘\n⬆️🔘⬆️🔘🔘🔘\n🔘🔘🔘🔘🔘⬆️\n🔘🔘🔵🔘🔘🔘\n🔘🔘🔘🔘🔘🔵";
    ASSERT_EQ(expected_solving, solve_masyu(input_board));
}

TEST(Masyu, GenerateCorrectProblem) {
    std::string generated_problem = generate_correct_problem();
    ASSERT_TRUE(validate_board_string(generated_problem));
}

// Додаткові тести для функцій взаємодії з базою даних
TEST(Database, InsertProblem) {
    std::string test_problem = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B";
    ASSERT_TRUE(db_insert_problem(db, test_problem));
}

TEST(Database, SelectProblemById) {
    int problem_id = 1;
    std::string expected_problem = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B";
    ASSERT_EQ(expected_problem, db_select_problem_by_id(db, problem_id));
}

TEST(Database, SelectRandomProblem) {
    std::string random_problem = db_select_random_problem(db);
    ASSERT_TRUE(validate_board_string(random_problem));
}

TEST(Database, InsertSolvingToExistingProblem) {
    std::string test_problem = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B";
    std::string test_solving = "⬆️🔵⬆️🔘🔘🔘\n🔘🔘🔘🔵🔘🔘\n⬆️🔘⬆️🔘🔘🔘\n🔘🔘🔘🔘🔘⬆️\n🔘🔘🔵🔘🔘🔘\n🔘🔘🔘🔘🔘🔵";
    ASSERT_TRUE(db_insert_solving_to_existing_problem(db, test_problem, test_solving));
}