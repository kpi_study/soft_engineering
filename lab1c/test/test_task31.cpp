#include "task31.h"
#include "gtest/gtest.h"

TEST(CellLineTest, SetDirection) {
    CellLine cell({}, {});
    CellLine new_cell = cell.set_direction(Direction::UP);
    EXPECT_EQ(new_cell.is_set, (std::set<Direction>{Direction::UP}));
}

TEST(BoardTest, SetDirection) {
    std::unordered_map<std::pair<int, int>, char> circles = {{{1, 1}, 'W'}};
    Board board(3, 3, circles);
    Board new_board = board.set_direction(1, 1, Direction::UP);
    EXPECT_TRUE(new_board.cell_lines.at({1, 1}).is_set.count(Direction::UP));
}

TEST(BoardTest, Evolve) {
    std::unordered_map<std::pair<int, int>, char> circles = {{{1, 1}, 'W'}};
    Board board(3, 3, circles);
    Board new_board = board.set_direction(1, 1, Direction::UP);
    Board evolved_board = new_board.evolve(new_board.cell_lines);
    EXPECT_EQ(evolved_board.line_segments.size(), 1);
}

TEST(BoardTest, SolvedException) {
    std::string board_str = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B\n";
    auto board = board_from_string(board_str);
    try {
        solve(board);
        FAIL() << "Expected SolvedException";
    } catch (const SolvedException& exc) {
        SUCCEED();
    } catch (...) {
        FAIL() << "Expected SolvedException";
    }
}