#include <gtest/gtest.h>
#include "db_service.h"

// Налаштування підключення до бази даних
sql::Connection* connection = get_connection();

TEST(DBService, InsertProblem) {
    std::string test_problem = "1234567890";
    ASSERT_TRUE(insert_problem(connection, test_problem));
}

TEST(DBService, SelectProblemById) {
    int problem_id = 1;
    std::string expected_problem = "1234567890";
    ASSERT_EQ(expected_problem, select_problem_by_id(connection, problem_id));
}

TEST(DBService, SelectRandomProblem) {
    std::string random_problem = select_random_problem(connection);
    ASSERT_FALSE(random_problem.empty());
}

TEST(DBService, InsertSolvingToExistingProblemId) {
    int problem_id = 1;
    std::string test_solving = "0987654321";
    ASSERT_TRUE(insert_solving_to_existing_problem_id(connection, problem_id, test_solving));
}

TEST(DBService, InsertSolvingToExistingProblem) {
    std::string test_problem = "1234567890";
    std::string test_solving = "0987654321";
    ASSERT_TRUE(insert_solving_to_existing_problem(connection, test_problem, test_solving));
}

TEST(DBService, SelectSolvingByExistingProblemId) {
    int problem_id = 1;
    std::vector<std::string> expected_solvings = {"0987654321"};
    ASSERT_EQ(expected_solvings, select_solving_by_existing_problem_id(connection, problem_id));
}

TEST(DBService, SelectSolvingByExistingProblem) {
    std::string test_problem = "1234567890";
    std::vector<std::string> expected_solvings = {"0987654321"};
    ASSERT_EQ(expected_solvings, select_solving_by_existing_problem(connection, test_problem));
}

TEST(DBService, AddUser) {
    std::string test_username = "testuser";
    std::string test_password = "password123";
    std::string test_developer_key = "abcdefghijklmnop";
    ASSERT_TRUE(add_user(connection, test_username, test_password, test_developer_key));
}

TEST(DBService, CheckUserExists) {
    std::string existing_username = "testuser";
    std::string nonexisting_username = "fakeuser";
    ASSERT_TRUE(check_user_exists(connection, existing_username));
    ASSERT_FALSE(check_user_exists(connection, nonexisting_username));
}

TEST(DBService, GetAllUsers) {
    std::vector<std::map<std::string, std::string>> users = get_all_users(connection);
    ASSERT_FALSE(users.empty());
}

TEST(DBService, GetUserByUsername) {
    std::string test_username = "testuser";
    std::map<std::string, std::string> user_db = get_user_by_username(connection, test_username);
    ASSERT_EQ(test_username, user_db["username"]);
}