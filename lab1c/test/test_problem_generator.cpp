#include "problem_generator.h"
#include "gtest/gtest.h"

TEST(ProblemGeneratorTest, GenerateRandomMasyu) {
    std::string board_str = generate_random_masyu(4, 4, 2, 2);
    EXPECT_EQ(board_str.count('W'), 2);
    EXPECT_EQ(board_str.count('B'), 2);
}

TEST(ProblemGeneratorTest, IsSolvable) {
    std::string solvable_board = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B\n";
    std::string unsolvable_board = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....W\n";
    EXPECT_TRUE(is_solvable(solvable_board));
    EXPECT_FALSE(is_solvable(unsolvable_board));
}

TEST(ProblemGeneratorTest, GenerateAndTest) {
    std::string board_str = generate_and_test(4, 4, 2, 2);
    EXPECT_TRUE(is_solvable(board_str));
}