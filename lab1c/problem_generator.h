#ifndef PROBLEM_GENERATOR_H
#define PROBLEM_GENERATOR_H

#include <string>

std::string generate_random_masyu(int width, int height, int num_white, int num_black);
bool is_solvable(const std::string& board_str);
std::string generate_and_test(int width, int height, int num_white, int num_black);

#endif // PROBLEM_GENERATOR_H