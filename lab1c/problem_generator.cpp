/*
Module for generating random problems
*/
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <random>
#include "task31.h"

std::string generate_random_masyu(int width, int height, int num_white, int num_black) {
    /*
    Generates random board string
    :param width:
    :param height:
    :param num_white:
    :param num_black:
    :return: str
    */
    std::vector<std::vector<char>> board(height, std::vector<char>(width, '.'));

    std::vector<std::pair<int, int>> all_positions;
    for (int x = 0; x < width; ++x) {
        for (int y = 0; y < height; ++y) {
            all_positions.emplace_back(x, y);
        }
    }

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(all_positions.begin(), all_positions.end(), g);

    std::vector<std::pair<int, int>> white_positions(all_positions.begin(), all_positions.begin() + num_white);
    std::vector<std::pair<int, int>> black_positions(all_positions.begin() + num_white, all_positions.begin() + num_white + num_black);

    for (const auto& pos : white_positions) {
        board[pos.second][pos.first] = 'W';
    }

    for (const auto& pos : black_positions) {
        board[pos.second][pos.first] = 'B';
    }

    std::string result;
    for (const auto& row : board) {
        result += std::string(row.begin(), row.end()) + "\n";
    }

    return result;
}

bool is_solvable(const std::string& board_str) {
    /*
    Checks is problem string solvable
    :param board_str:
    :return: bool
    */
    auto board = board_from_string(board_str);
    auto solved_board = solve(board);
    return solved_board.has_value();
}

std::string generate_and_test(int width, int height, int num_white, int num_black) {
    /*
    Runs random generation process
    :param width:
    :param height:
    :param num_white:
    :param num_black:
    :return: str
    */
    while (true) {
        std::string puzzle = generate_random_masyu(width, height, num_white, num_black);
        if (is_solvable(puzzle)) {
            return puzzle;
        }
    }
}



