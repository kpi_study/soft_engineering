#include "db_service.h"
#include "config.h"
#include <iostream>

sql::Connection* get_connection() {
    try {
        sql::mysql::MySQL_Driver* driver = sql::mysql::get_mysql_driver_instance();
        sql::Connection* connection = driver->connect(HOST, USER, PASSWORD);
        connection->setSchema(DB_NAME);
        std::cout << "- DB: Successfully connected!" << std::endl;
        return connection;
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Connection error!" << std::endl;
        std::cout << e.what() << std::endl;
        return nullptr;
    }
}

bool insert_problem(sql::Connection* connection, const std::string& problem_str) {
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("INSERT INTO masyu_problems (problem) VALUES (?)"));
        pstmt->setString(1, problem_str);
        pstmt->executeUpdate();
        std::cout << "- DB: Inserted:\n'" << problem_str << "'" << std::endl;
        return true;
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Insertion error!" << std::endl;
        std::cout << e.what() << std::endl;
        return false;
    }
}

std::string select_problem_by_id(sql::Connection* connection, int problem_id) {
    std::string problem_string;
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("SELECT problem FROM masyu_problems WHERE id = ?"));
        pstmt->setInt(1, problem_id);
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        if (res->next()) {
            problem_string = res->getString("problem");
            std::cout << "- DB: Selected:\n" << problem_string << std::endl;
        } else {
            std::cout << "- DB: No problem found for the given ID: " << problem_id << std::endl;
        }
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Selection error!" << std::endl;
        std::cout << e.what() << std::endl;
    }
    return problem_string;
}

std::string select_random_problem(sql::Connection* connection) {
    std::string problem_string;
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("SELECT problem FROM masyu_problems ORDER BY RAND() LIMIT 1"));
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        if (res->next()) {
            problem_string = res->getString("problem");
            std::cout << "- DB: Selected:\n" << problem_string << std::endl;
        } else {
            std::cout << "- DB: No random problem found" << std::endl;
        }
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Selection error!" << std::endl;
        std::cout << e.what() << std::endl;
    }
    return problem_string;
}

bool insert_solving_to_existing_problem_id(sql::Connection* connection, int problem_id, const std::string& solving_str) {
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("INSERT INTO masyu_solvings (problem_id, solving) VALUES (?, ?)"));
        pstmt->setInt(1, problem_id);
        pstmt->setString(2, solving_str);
        pstmt->executeUpdate();
        std::cout << "- DB: Solving added:\n'" << solving_str << "'" << std::endl;
        return true;
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Insertion error!" << std::endl;
        std::cout << e.what() << std::endl;
        return false;
    }
}

bool insert_solving_to_existing_problem(sql::Connection* connection, const std::string& problem, const std::string& solving_str) {
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt_select(connection->prepareStatement("SELECT id FROM masyu_problems WHERE problem = ?"));
        pstmt_select->setString(1, problem);
        std::unique_ptr<sql::ResultSet> res(pstmt_select->executeQuery());
        if (res->next()) {
            int problem_id = res->getInt("id");
            return insert_solving_to_existing_problem_id(connection, problem_id, solving_str);
        } else {
            std::cout << "- DB: Problem not found for the given string: " << problem << std::endl;
            return false;
        }
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Insertion error!" << std::endl;
        std::cout << e.what() << std::endl;
        return false;
    }
}

std::vector<std::string> select_solving_by_existing_problem_id(sql::Connection* connection, int problem_id) {
    std::vector<std::string> solvings;
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("SELECT distinct solving FROM masyu_solvings WHERE problem_id = ?"));
        pstmt->setInt(1, problem_id);
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        while (res->next()) {
            solvings.push_back(res->getString("solving"));
        }
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Selection error!" << std::endl;
        std::cout << e.what() << std::endl;
    }
    return solvings;
}

std::vector<std::string> select_solving_by_existing_problem(sql::Connection* connection, const std::string& problem) {
    std::vector<std::string> solvings;
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement(
            "SELECT distinct solving FROM masyu_solvings "
            "JOIN masyu_problems ON masyu_solvings.problem_id = masyu_problems.id "
            "WHERE problem = ?"));
        pstmt->setString(1, problem);
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        while (res->next()) {
            solvings.push_back(res->getString("solving"));
        }
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Selection error!" << std::endl;
        std::cout << e.what() << std::endl;
    }
    return solvings;
}

bool add_user(sql::Connection* connection, const std::string& username, const std::string& password_str, const std::string& developer_key) {
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement(
            "INSERT INTO users (username, hashed_password, developer_key) VALUES (?, ?, ?)"));
        pstmt->setString(1, username);
        pstmt->setString(2, password_str);
        pstmt->setString(3, developer_key);
        pstmt->executeUpdate();
        return true;
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Insertion error!" << std::endl;
        std::cout << e.what() << std::endl;
        return false;
    }
}

bool check_user_exists(sql::Connection* connection, const std::string& username) {
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("SELECT * FROM users WHERE username = ?"));
        pstmt->setString(1, username);
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        return res->next();
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Selection error!" << std::endl;
        std::cout << e.what() << std::endl;
        return false;
    }
}

std::vector<std::map<std::string, std::string>> get_all_users(sql::Connection* connection) {
    std::vector<std::map<std::string, std::string>> users;
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("SELECT * FROM users"));
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        while (res->next()) {
            std::map<std::string, std::string> user;
            user["id"] = res->getString("id");
            user["username"] = res->getString("username");
            user["hashed_password"] = res->getString("hashed_password");
            user["developer_key"] = res->getString("developer_key");
            users.push_back(user);
        }
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Selection error!" << std::endl;
        std::cout << e.what() << std::endl;
    }
    return users;
}

std::map<std::string, std::string> get_user_by_username(sql::Connection* connection, const std::string& username) {
    std::map<std::string, std::string> user_db;
    try {
        std::unique_ptr<sql::PreparedStatement> pstmt(connection->prepareStatement("SELECT * FROM users WHERE username = ?"));
        pstmt->setString(1, username);
        std::unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
        if (res->next()) {
            user_db["id"] = res->getString("id");
            user_db["username"] = res->getString("username");
            user_db["hashed_password"] = res->getString("hashed_password");
            user_db["developer_key"] = res->getString("developer_key");
        }
    } catch (sql::SQLException& e) {
        std::cout << "- DB: Selection error!" << std::endl;
        std::cout << e.what() << std::endl;
    }
    return user_db;
}

