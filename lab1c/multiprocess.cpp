#include <pthread.h>
#include <vector>
#include <queue>

struct WorkItem {
    std::shared_ptr<Lookahead> lookahead;
    std::deque<std::shared_ptr<PossibilityPair>> possibilities;
};

std::queue<WorkItem> work_queue;
std::mutex work_queue_mutex;
std::condition_variable work_queue_cv;

void* worker_thread(void* arg) {
    while (true) {
        WorkItem item;
        {
            std::unique_lock<std::mutex> lock(work_queue_mutex);
            work_queue_cv.wait(lock, [&]{ return !work_queue.empty(); });
            item = work_queue.front();
            work_queue.pop();
        }

        item.possibilities = get_possibility_list(item.lookahead);

        // Merge the possibilities back into the original Lookahead object
        {
            std::lock_guard<std::mutex> lock(item.lookahead->mutex);
            item.lookahead->possibilities.insert(item.lookahead->possibilities.end(),
                                                 item.possibilities.begin(),
                                                 item.possibilities.end());
        }
    }
    return nullptr;
}

void expand(std::shared_ptr<Lookahead>& lookahead) {
    const int num_threads = std::thread::hardware_concurrency();
    std::vector<pthread_t> threads(num_threads);

    // Create worker threads
    for (int i = 0; i < num_threads; ++i) {
        pthread_create(&threads[i], nullptr, worker_thread, nullptr);
    }

    // Split the work and add it to the work queue
    {
        std::lock_guard<std::mutex> lock(work_queue_mutex);
        for (const auto& pos : get_possibility_list(lookahead)) {
            work_queue.push({pos->yes, {}});
            work_queue.push({pos->no, {}});
        }
    }
    work_queue_cv.notify_all();

    // Wait for all worker threads to finish
    for (int i = 0; i < num_threads; ++i) {
        pthread_join(threads[i], nullptr);
    }

    // Merge the possibilities from the work queue
    while (!work_queue.empty()) {
        std::lock_guard<std::mutex> lock(lookahead->mutex);
        lookahead->possibilities.insert(lookahead->possibilities.end(),
                                        work_queue.front().possibilities.begin(),
                                        work_queue.front().possibilities.end());
        work_queue.pop();
    }
}