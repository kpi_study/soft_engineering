import { useState } from 'react'
import './App.css'
import { Tab, Tabs, Nav } from 'react-bootstrap';
import axios from 'axios';

// npm run dev

function App() {
  const [problem, setProblem] = useState('');
  const [problemboard, setProblemBoard] = useState('');

  const fetchProblem = async () => {
    try {
      const response = await axios.get('http://localhost:8000/generate');
      setProblem(response.data.problem.replaceAll('\n', '\\n'));
      setProblemBoard(response.data.problemboard)
      console.log('New problem:', response.data.problemboard);
    } catch (error) {
      console.error('Error fetching problem:', error);
    }
  };

  const [inputString, setInputString] = useState('');
  const [solving, setSolving] = useState('');

  const handleInputChange = (event) => {
    setInputString(event.target.value);
  };

  const handleSolve = async (e) => {
    e.preventDefault();
    console.log('String: ', inputString )
    try {
      const response = await axios.post('http://localhost:8000/solve?problem_str=' + inputString);
      print('RESPONCE')
      print(response.data.solving)
      setSolving(response.data.solving.replaceAll('\\n', '\n'));
    } catch (error) {
      console.error('Error fetching solution:', error);
    }
  };

  return (
    <>
      <h1 className='text-center mt-5'>Masyu Solver</h1>
      <div className="container">

        <h2>Generator</h2>

        <div className="block d-flex flex-column">
          <button onClick={fetchProblem} className="btn btn-primary mb-3" style={{ width: '200px' }}>Generate Problem</button>

          String: {problem && (
            <div><pre>{problem}</pre></div>
          )}

          Board:
          {problemboard && (
            <div><pre>{problemboard}</pre></div>
          )}
        </div>

        <h2>Solver</h2>

        <div className="block">
          <div className="input-group mb-3">
            <span className="input-group-text" id="inputGroup-sizing-default">Input string</span>
            <input type="text" className="form-control" 
            aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"
            value={inputString} onChange={handleInputChange}/>
          </div>

          <button onClick={handleSolve} className="btn btn-primary mb-3" style={{ width: '200px' }}>Solve</button>

          {solving && (
            <div className="solution mb-5">
              {solving.split('\n').map((line, index) => (
                <div key={index}>{line}</div>
              ))}
            </div>
          )}
        </div>
      </div>
    </>
  )
}

export default App
