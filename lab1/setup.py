from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='masyu-solver',
    version='0.1',
    author='Anastasiia Poliakova',
    author_email='nastyapolyakova02@gmail.comm',
    description='Package provides an algorithm for solving masyu and generating random problem with REST API',
    packages=find_packages(),
    install_requires=requirements,
)
