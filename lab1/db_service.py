"""
Модуль для роботи з бд
"""
import pymysql
from config import HOST, USER, PASSWORD, DB_NAME


def get_connection():
    """
    Функція для встановлення зʼєднання з бд.
    Args:
    Returns:
    """
    try:
        connection = pymysql.connect(
            host=HOST,
            port=3306,
            user=USER,
            password=PASSWORD,
            database=DB_NAME,
            cursorclass=pymysql.cursors.DictCursor
        )
        print("- DB: Successfully connected!")
        return connection
    except pymysql.Error as ex:
        print("- DB: Connection error!")
        print(ex)
        return None


def insert_problem(connection, problem_str: str):
    """
    Функція для додавання умови до бд.
    Args:
        connection
        problem_str: str
    Returns:
        bool
    """
    try:
        with connection.cursor() as cursor:
            insert_query = "INSERT INTO `masyu_problems` (problem) VALUES ('" + problem_str + "');"
            cursor.execute(insert_query)
            connection.commit()
            print("- DB: Inserted:\n'" + problem_str + "'")
    except pymysql.Error as ex:
        print("- DB: Insertion error!")
        print(ex)
        return False
    finally:
        connection.close()
    return True


def select_problem_by_id(connection, problem_id: int):
    """
    Функція для вибору умови до бд.
    Args:
        connection
        problem_id: int
    Returns:
        str
    """
    problem_string = ""
    try:
        with connection.cursor() as cursor:
            select_query = "SELECT problem FROM `masyu_problems` WHERE id=" + str(problem_id) + ";"
            cursor.execute(select_query)
            result = cursor.fetchone()
            if result is not None:
                problem_string = result['problem']
                print("- DB: Selected:")
                print(problem_string)
            else:
                print("- DB: No problem found for the given ID:", problem_id)
    except pymysql.Error as ex:
        print("- DB: Selection error!")
        print(ex)
    finally:
        connection.close()
    return problem_string


def select_random_problem(connection):
    """
    Функція для вибору випадкової умови з бд.
    Args:
        connection
    Returns:
        str
    """
    problem_string = ""
    try:
        with connection.cursor() as cursor:
            select_query = "SELECT * FROM `masyu_problems` ORDER BY RAND() LIMIT 1;"
            cursor.execute(select_query)
            result = cursor.fetchone()
            if result is not None:
                problem_string = result['problem']
                print("- DB: Selected:")
                print(result)
            else:
                print("- DB: No random problem found")
    except pymysql.Error as ex:
        print("- DB: Selection error!")
        print(ex)
    finally:
        connection.close()
    return problem_string


def insert_solving_to_existing_problem_id(connection, problem_id: int, solving_str: str):
    """
    Функція для додавання рішення до умови за ід у бд.
    Args:
        connection
        problem_id: int
        solving_str: str
    Returns:
        bool
    """
    try:
        with connection.cursor() as cursor:
            insert_query = ("INSERT INTO `masyu_solvings` (problem_id, solving) "
                            "VALUES ('") + str(problem_id) + "', '" + solving_str + "');"
            cursor.execute(insert_query)
            connection.commit()
            print("- DB: Solving added:\n'" + solving_str + "'")
    except pymysql.Error as ex:
        print("- DB: Inserting error!")
        print(ex)
        return False
    finally:
        connection.close()
    return True


def insert_solving_to_existing_problem(connection, problem: str, solving_str: str):
    """
    Функція для додавання рішення до умови у бд.
    Args:
        connection
        problem: str
        solving_str: str
    Returns:
        bool
    """
    try:
        with connection.cursor() as cursor:
            select_query = "SELECT id FROM `masyu_problems` WHERE problem='" + problem + "';"
            cursor.execute(select_query)
            result = cursor.fetchone()
            if result is not None:
                problem_id = result['id']
                insert_query = ("INSERT INTO `masyu_solvings` (problem_id, solving) "
                                "VALUES ('") + str(problem_id) + "', '" + solving_str + "');"
                cursor.execute(insert_query)
                connection.commit()
                print("- DB: Solving added:\n'" + solving_str + "'")
    except pymysql.Error as ex:
        print("- DB: Inserting error!")
        print(ex)
        return False
    finally:
        connection.close()
    return True


def select_solving_by_existing_problem_id(connection, problem_id: int):
    """
    Функція для вибору рішення по ід умови у бд.
    Args:
        connection
        problem_id: int
    Returns:
        str
    """
    result = ""
    try:
        with connection.cursor() as cursor:
            select_query = ("SELECT distinct solving FROM `masyu_solvings` "
                            "WHERE problem_id=") + str(problem_id) + ";"
            cursor.execute(select_query)
            result = cursor.fetchall()
    except pymysql.Error as ex:
        print("- DB: Selection error!")
        print(ex)
    finally:
        connection.close()
    return result


def select_solving_by_existing_problem(connection, problem: str):
    """
    Функція для вибору рішення по умови у бд.
    Args:
        connection
        problem: str
    Returns:
        str
    """
    result = ""
    try:
        with connection.cursor() as cursor:
            select_query = ("SELECT distinct solving FROM `masyu_solvings` "
                            "JOIN `masyu_problems` ON masyu_solvings.problem_id=masyu_problems.id "
                            "WHERE problem='" + problem + "';")
            cursor.execute(select_query)
            result = cursor.fetchall()
    except pymysql.Error as ex:
        print("- DB: Selection error!")
        print(ex)
    finally:
        connection.close()
    return result


P = (".W....\n"
     "...B..\n"
     "W.W...\n"
     ".....W\n"
     "..B...\n"
     ".....B")
S = ("┌─┬─┬─┬─┬─┬─┐\n"
     "│┌─W───────┐│\n"
     "├│┼─┼─┼─┼─┼│┤\n"
     "│││┌─┐│B───┘│\n"
     "├│┼│┼│┼│┼─┼─┤\n"
     "│W│││W│││┌─┐│\n"
     "├│┼│┼│┼│┼│┼│┤\n"
     "│└─┘│││││││W│\n"
     "├─┼─┼│┼│┼│┼│┤\n"
     "│┌───B│└─┘│││\n"
     "├│┼─┼─┼─┼─┼│┤\n"
     "│└─────────B│\n"
     "└─┴─┴─┴─┴─┴─┘")
# insert_problem(p)
# select_problem_by_id(22)
# insert_solving_to_existing_problem(p, s)
# insert_solving_to_existing_problem_id(22, s)
# select_solving_by_existing_problem_id(22)
# select_solving_by_existing_problem(p)


def add_user(connection, username, password_str, developer_key):
    """
    Функція для додавання нового юзера у бд.
    Args:
        connection
        username
        password_str
        developer_key
    Returns:
        bool
    """
    try:
        with connection.cursor() as cursor:
            # Додавання нового користувача
            sql = ("INSERT INTO users (username, hashed_password, developer_key) VALUES "
                   "('") + username + "', '" + password_str + "', '" + developer_key + "')"
            cursor.execute(sql)
            connection.commit()
    except pymysql.Error as ex:
        print("- DB: Insertion error!")
        print(ex)
        return False
    finally:
        connection.close()
    return True


def check_user_exists(connection, username):
    """
    Функція для перевірки існування юзера у бд.
    Args:
        connection
        username
    Returns:
        bool
    """
    try:
        with connection.cursor() as cursor:
            # Пошук користувача за ім'ям
            sql = "SELECT * FROM users WHERE username = '" + username + "';"
            cursor.execute(sql)
            result = cursor.fetchone()
            return result is not None
    except pymysql.Error as ex:
        print("- DB: Selection error!")
        print(ex)
        return False
    finally:
        connection.close()


def get_all_users(connection):
    """
    Функція для отримання усіх юзерів у бд.
    Args:
        connection
    Returns:
        bool
    """
    result = ""
    try:
        with connection.cursor() as cursor:
            # Вибірка усіх користувачів
            sql = "SELECT * FROM users"
            cursor.execute(sql)
            result = cursor.fetchall()
    except pymysql.Error as ex:
        print("- DB: Selection error!")
        print(ex)
    finally:
        connection.close()
    return result


# Метод для отримання користувача за його ім'ям (username)
def get_user_by_username(connection, username: str):
    """
    Функція для отримання юзера за іменем у бд.
    Args:
        connection
        username
    Returns:
        bool
    """
    try:
        with connection.cursor() as cursor:
            # Вибірка користувача за ім'ям (username)
            sql = "SELECT * FROM users WHERE username = '" + username + "';"
            cursor.execute(sql)
            result = cursor.fetchone()
            if result:
                user_db = {
                    "id": result['id'],
                    "username": result['username'],
                    "hashed_password": result['hashed_password'],
                    "developer_key": result['developer_key']
                }
                return user_db
            return None
    except pymysql.Error as ex:
        print("- DB: Selection error!")
        print(ex)
        return None
    finally:
        connection.close()
