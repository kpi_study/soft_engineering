"""
Module to release multiprocessing
"""
import multiprocessing as mp
from typing import List

from task31 import (solve, Board, board_from_string,
                    print_big_board)


def solve_subboard(subboard):
    """
    Алгоритм для вирішення підзадачі
    :param subboard:
    :return: Board
    """
    return solve(subboard)


def solve_masyu_parallel(board, num_processes):
    """
    Алгоритм паралельного обчислення головоломки
    :param board:
    :param num_processes:
    :return: Board
    """
    # Розділити вхідну дошку на підзадачі
    subboards = split_board(board, num_processes)

    # Створити пул процесів
    with mp.Pool(processes=num_processes) as pool:
        # Відправити підзадачі на паралельне вирішення
        results = pool.map(solve_subboard, subboards)

    # Зібрати результати воєдино
    solved_board = combine_results(results)

    return solved_board


def split_board(board, num_processes):
    """
    Логіка для розділення вхідної дошки на підзадачі
    :param board:
    :param num_processes:
    :return: List<Board>
    """
    subboard_height = board.height // 2
    subboard_width = board.width // 2
    print(subboard_height, subboard_width)

    subboards = []
    for i in range(num_processes):
        row_start = (i // 2) * subboard_height
        row_end = row_start + subboard_height if i // 2 < 1 else board.height
        col_start = (i % 2) * subboard_width
        col_end = col_start + subboard_width if i % 2 == 0 else board.width

        subboard_circles = {}
        subboard_cell_lines = {(x, y): board.cell_lines[x, y]
                               for x in range(col_start, col_end)
                               for y in range(row_start, row_end)}

        for x in range(col_start, col_end):
            for y in range(row_start, row_end):
                if (x, y) in board.circles:
                    subboard_circles[x - col_start, y - row_start] = board.circles[x, y]

        subboard = Board(subboard_width, subboard_height, subboard_circles, subboard_cell_lines)
        subboards.append(subboard)

    return subboards


def combine_results(results: List[Board]):
    """
    Логіка для поєднання результатів вирішення підзадач
    :param results: List<Board>
    :return: Board
    """
    width = sum(subboard.width for subboard in results[:2])
    height = sum(subboard.height for subboard in results[::2])
    circles = {}
    cell_lines = {}

    row_offset = 0
    col_offset = 0
    for i, subboard in enumerate(results):
        for x in range(subboard.width):
            for y in range(subboard.height):
                circles[x + col_offset, y + row_offset] = subboard.circles[x, y]
                cell_lines[x + col_offset, y + row_offset] = subboard.cell_lines[x, y]

        if i % 2 == 1:
            row_offset += subboard.height
            col_offset = 0
        else:
            col_offset += subboard.width

    return Board(width, height, circles, cell_lines)


def main():
    """
    Function main to run program.
    Returns:
        void
    """
    board_str = ".W....\n" + "...B..\n" + "W.W...\n" + ".....W\n" + "..B...\n" + ".....B\n"
    board = board_from_string(board_str)
    print(print_big_board(board))
    solved_board = solve_masyu_parallel(board, 4)
    if solved_board:
        print(print_big_board(solved_board))


if __name__ == '__main__':
    main()
