"""
Module for processing HTTP requests.
"""
import random
import re
import textwrap
import secrets
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from passlib.context import CryptContext
from task31 import board_from_string, solve, print_big_board
from db_service import get_connection
import db_service as db


app = FastAPI()
# uvicorn main:app --reload
# pylint *.py
# coverage run -m pytest test_main.py test_task31.py
# coverage report --show-missing

# Додаємо CORSMiddleware для дозволу запитів з localhost:3000
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:3000"],
    allow_credentials=True,
    allow_methods=["GET", "POST"],
    allow_headers=["*"],
)


def generate_correct_problem():
    """
    Function for generation correct conditions of the problem.
    Args:
    Returns:
        str: string with condition.
    """
    board_str = [".W....\n...B..\nW.W...\n.....W\n..B...\n.....B",
                 ".....B\n..B...\n....WW\n......\n...B..\nB...W.",
                 "....W.\nW.....\n.....B\nB.....\n.....W\n......",
                 "....W.\nW.....\n.B....\n..B..W\n......\n.W...B",
                 ".W..WB\n......\n.W.W..\n....B.\n.....W\n..W..."]
    return random.choices(board_str)[0]


# Валідація вхідних даних
def validate_board_string(board_str):
    """
    Function for validation of input data.

    Args:
        board_str (str): input string.

    Returns:
        bool: valid or not.
    """
    board_str = board_str.replace("\\n", "\n")
    board_str = textwrap.dedent(board_str.rstrip().strip("\n"))
    # Перевірка чи рядок має правильний формат
    if not re.match(r'^([.WB]*\n)+[.WB]*$', board_str):
        print("-- Wrong format!")
        return False

    # Розділити рядок на окремі рядки
    lines = board_str.split('\n')

    # Перевірка чи кожен рядок має однакову довжину (кількість символів)
    first_line_length = len(lines[0])
    if not all(len(line) == first_line_length for line in lines):
        print("-- Lines not equal!")
        return False

    # Перевірка мінімальної кількості рядків і символів у кожному рядку
    if len(lines) < 2 or first_line_length < 2:
        print("-- <2 lines or symbols")
        return False

    return True


# Функція для вирішення головоломки Masyu
def solve_masyu(board_str):
    """
    Function to run solving the puzzle of Masyu.

    Args:
        board_str (str): input string.

    Returns:
        void
    """
    board = board_from_string(board_str)
    print(print_big_board(board))
    try:
        solved_board = solve(board)
        print(print_big_board(solved_board))
        if solved_board:
            return print_big_board(solved_board)
        raise ValueError("Unable to solve the puzzle")
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e)) from e


@app.post("/solve")
def get_solution(problem_str: str):
    """
    A POST endpoint to solve Masyu problem.

    Parameters:
    - name: board_str.
      required: true.
      description: Board to solve.

    Responses:
    - 200:
        description: A solution.
    """
    problem_str = problem_str.replace("\\n", "\n")

    if validate_board_string(problem_str):
        db.insert_problem(get_connection(), problem_str)
        solving = solve_masyu(problem_str)
        db.insert_solving_to_existing_problem(get_connection(), problem_str, solving)
        return {"solving": solving}
    raise HTTPException(status_code=400,
                        detail=str("Invalid input data. Only . W B \\n symbols are allowed. "
                                   ">2 in each line, >2 lines. Lines are equal length."))


@app.get("/generate")
def generate():
    """
    A GET endpoint that generates Masyu problem.

    Parameters:

    Responses:
    - 200:
        description: A problem.
    """
    problem = db.select_random_problem(get_connection())
    problem_board = print_big_board(board_from_string(problem))
    return {"problem": problem, "problemboard": problem_board}


@app.get("/getproblem")
def get_problem_by_id(problem_id: int):
    """
    Function for getting problem by id.
    Args:
        problem_id: int
    Returns:
        json
    """
    problem = db.select_problem_by_id(get_connection(), problem_id)
    problem_board = print_big_board(board_from_string(problem))
    return {"problem": problem, "problemboard": problem_board}


@app.post("/addproblem")
def add_problem(problem_str: str):
    """
    Function for adding problem.
    Args:
        problem_str: str
    Returns:
        json
    """
    problem_str = problem_str.replace("\\n", "\n")
    if validate_board_string(problem_str):
        db.insert_problem(get_connection(), problem_str)
        return {"problem": problem_str}
    raise HTTPException(status_code=400,
                        detail=str("Invalid input data. Only . W B \\n symbols are allowed. "
                                   ">2 in each line, >2 lines. Lines are equal length."))


@app.get("/get-solvings-by-id")
def get_solvings_by_problem_id(problem_id: int):
    """
    Function for getting solving by problem id.
    Args:
        problem_id: int
    Returns:
        json
    """
    solvings = db.select_solving_by_existing_problem_id(get_connection(), problem_id)
    if solvings is not None:
        result = []
        for solving in solvings:
            solving_string = solving['solving']
            result.append({"solving": solving_string})
        return {"solvings": result}
    return {"solvings": []}


@app.get("/get-solvings-by-problem")
def get_solvings_by_problem(problem_str: str):
    """
    Function for getting solving by problem.
    Args:
        problem_str: str
    Returns:
        json
    """
    solvings = db.select_solving_by_existing_problem(get_connection(), problem_str)
    if solvings is not None:
        result = []
        for solving in solvings:
            solving_string = solving['solving']
            result.append({"solving": solving_string})
        return {"solvings": result}
    return {"solvings": []}


@app.post("/add-solving-by-id")
def add_solving_by_problem_id(problem_id: int, solving_str: str):
    """
    Function for adding solving to problem id.
    Args:
        problem_id: int
        solving_str: str
    Returns:
        json
    """
    if db.insert_solving_to_existing_problem_id(get_connection(), problem_id, solving_str):
        return {"problem_id": problem_id, "solving": solving_str}
    raise HTTPException(status_code=400, detail="Помилка додавання нової умови")


@app.post("/add-solving-by-problem")
def add_solving_by_problem(problem: str, solving_str: str):
    """
    Function for adding solving to problem.
    Args:
        problem: str
        solving_str: str
    Returns:
        json
    """
    if db.insert_solving_to_existing_problem(get_connection(), problem, solving_str):
        return {"problem_str": problem, "solving": solving_str}
    raise HTTPException(status_code=400, detail="Помилка додавання нової умови")


# Контекст для хешування паролів
pwd_context = CryptContext(schemes=["scram"], deprecated="auto")


def verify_password(plain_password, hashed_password):
    """
    Function for password verifying
    Args:
        plain_password: str
        hashed_password: str
    Returns:
        bool
    """
    return pwd_context.verify(plain_password, hashed_password)


def verify_key(developer_key, user_developer_key):
    """
    Function for key verifying
    Args:
        developer_key: str
        user_developer_key: str
    Returns:
        bool
    """
    return developer_key == user_developer_key


def get_password_hash(password_str: str):
    """
    Function for getting hash code
    Args:
        password_str: str
    Returns:
        str
    """
    return pwd_context.hash(password_str)


@app.post("/register")
async def register(username: str, password_str: str):
    """
    Function for HTTP request for registration
    Args:
        username: str
        password_str: str
    Returns:
        json
    """
    if db.check_user_exists(get_connection(), username):
        raise HTTPException(status_code=400, detail="Користувач з таким ім'ям вже існує")

    hashed_password = get_password_hash(password_str)
    # Генеруємо новий девелоперський ключ
    developer_key = generate_developer_key()
    if db.add_user(get_connection(), username, hashed_password, developer_key):
        return {"message": "Реєстрація успішна", "developer_key": developer_key}
    raise HTTPException(status_code=400, detail="Помилка реєстрації")


# Функція для перевірки пароля під час авторизації
def authenticate_user_by_password(username: str, password_str: str):
    """
    Function for user password authentication
    Args:
        username: str
        password_str: str
    Returns:
        bool
    """
    if db.check_user_exists(get_connection(), username):
        user_db = db.get_user_by_username(get_connection(), username)
        if verify_password(password_str, user_db["hashed_password"]):
            return True
    return False


def authenticate_user_by_key(username: str, developer_key: str):
    """
    Function for user key authentication
    Args:
        username: str
        developer_key: str
    Returns:
        bool
    """
    if db.check_user_exists(get_connection(), username):
        user_db = db.get_user_by_username(get_connection(), username)
        if verify_key(developer_key, user_db["developer_key"]):
            return True
    return False


@app.post("/login")
async def login(username: str, password_str: str, developer_key: str):
    """
    Function for HTTP request for login
    Args:
        username: str
        password_str: str
        developer_key: str
    Returns:
        json
    """
    if developer_key is not None:
        # Авторизація за допомогою девелоперського ключа
        if authenticate_user_by_key(username, developer_key):
            return {"message": "Успішний вхід за допомогою девелоперського ключа"}
    # Авторизація за допомогою пароля
    if authenticate_user_by_password(username, password_str):
        return {"message": "Успішний вхід за допомогою пароля"}
    raise HTTPException(status_code=401, detail="Неправильне ім'я користувача або пароль або ключ")


# Функція для генерації девелоперського ключа
def generate_developer_key():
    """
    Function for key generating:
    Returns:
        str
    """
    return secrets.token_urlsafe(32)  # Генеруємо випадковий ключ
