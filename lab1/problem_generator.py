"""
Module for generating random problems
"""
import random
from task31 import (board_from_string, solve)


def generate_random_masyu(width, height, num_white, num_black):
    """
    Generates random board string
    :param width:
    :param height:
    :param num_white:
    :param num_black:
    :return: str
    """
    board = [['.' for _ in range(width)] for _ in range(height)]

    all_positions = [(x, y) for x in range(width) for y in range(height)]
    random.shuffle(all_positions)

    white_positions = all_positions[:num_white]
    black_positions = all_positions[num_white:num_white + num_black]

    for x, y in white_positions:
        board[y][x] = 'W'

    for x, y in black_positions:
        board[y][x] = 'B'

    return '\n'.join(''.join(row) for row in board)


def is_solvable(board_str):
    """
    Checks is problem string solvable
    :param board_str:
    :return: bool
    """
    board = board_from_string(board_str)
    # try:
    solved_board = solve(board)
    return solved_board is not None
    # except Exception:
    # return False


def generate_and_test(width, height, num_white, num_black):
    """
    Runs randon generation process
    :param width:
    :param height:
    :param num_white:
    :param num_black:
    :return: str
    """
    while True:
        puzzle = generate_random_masyu(width, height, num_white, num_black)
        if is_solvable(puzzle):
            return puzzle


# Example usage:
# width, height = 6, 6
# num_white, num_black = 3, 3
# solvable_puzzle = generate_and_test(width, height, num_white, num_black)
# print("Solvable puzzle found:")
# print(solvable_puzzle)
