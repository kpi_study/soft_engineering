""" ---------------------------------------------<Header>-
 Name: task31.py
 Title: Problem-solving algorithm
 Group: TV-32mp
 Student: Poliakova A.R.
 Written: 2024-03-11
 Revised: 2024-03-11
 Description: Намалюйте одну замĸнуту петлю, що сĸладається з вертиĸальних
та горизонтальних відрізĸів, таĸ, щоб вона проходила через
центр ĸожної ĸлітини лише один раз.
У ĸожній ĸлітці з білим ĸолом лінія повинна йти прямо через це
ĸоло і змінювати напрямоĸ на 90 градусів принаймні в одному із
сусідніх ĸвадратів.
У ĸожній ĸлітині з чорним ĸолом лінія повинна міняти напрямоĸ
на 90 градусів і йти прямо через обидві ĸлітини, що примиĸають
до ĸлітини з ĸолом.
 -------------------------------------------------</Header>-"""
import collections
import contextlib
import dataclasses
import enum
import textwrap
import weakref
from unittest.mock import sentinel
import attr
import frozendict


class Direction(enum.Enum):
    """
    An enumeration that represents possible directions (up, down, left, right).
    Used to determine the direction of the lines on the board.
    """
    UP = (0, -1)
    RIGHT = (1, 0)
    DOWN = (0, 1)
    LEFT = (-1, 0)

    def opposite(self):
        """
        Returns opposite direction
        """
        x, y = self.value
        return Direction((-x, -y))

    def turn_right(self):
        """
        Returns direction to the right
        """
        x, y = self.value
        return Direction((-y, x))

    def turn_left(self):
        """
        Returns direction to the left
        """
        x, y = self.value
        return Direction((y, -x))

    def move(self, x, y):
        """
        Moves coordinates by x y
        """
        dx, dy = self.value
        return x + dx, y + dy


class LoopException(Exception):
    """
    Indicates that we have created a loop. So either we solved the board or an error occurred
    """

    def __init__(self, loop_coords):
        super().__init__(loop_coords)
        self._loop_coords = loop_coords

    def validate_solved(self, circles):
        """
        Validation of solved board
        Args:
            self, circles
        Returns:
        """
        if not all(circle_coord in self._loop_coords for circle_coord in circles):
            raise ValueError("Closed loop does not contain all circles") from self


class SolvedException(Exception):
    """
    Indicates that the board is solved
    """

    def __init__(self, board):
        super().__init__()
        self.board = board


@dataclasses.dataclass(frozen=True)
class CellLine:
    """
    This class represents a line on a single cell.
    It has methods to set the line in a certain direction,
    setting a ban on the line in a certain direction,
    as well as checking the possibility of laying a line through the cell.
    """
    is_set: {Direction}  # Перелік напрямків, в яких лінія встановлена.
    cannot_set: {Direction}  # Перелік напрямків, в яких лінія не може бути встановлена.

    def set_direction(self, direction):
        """
        Sets a line in the specified direction and returns the result Cell Line.
        Args:
            self, direction
        Returns:
            CellLine: modified CellLine
        """
        if direction in self.is_set:
            return self
        if direction in self.cannot_set:
            raise ValueError(f"Can't set {direction} on cell {self}")

        is_set = self.is_set | {direction}
        cannot_set = self.cannot_set
        if len(is_set) == 2:
            cannot_set = frozenset(Direction) - is_set
        elif len(cannot_set) == 2:
            is_set = frozenset(Direction) - cannot_set
        return CellLine(is_set=frozenset(is_set), cannot_set=frozenset(cannot_set))

    def disallow_direction(self, direction):
        """
        Discards the ability to set a line in the given direction and returns the CellLine result.
        Args:
            self, direction
        Returns:
            CellLine: modified CellLine
        """
        if direction in self.cannot_set:
            return self
        if direction in self.is_set:
            raise ValueError(f"Can't disallow {direction} on cell {self}")

        cannot_set = self.cannot_set | {direction}
        is_set = self.is_set
        if len(cannot_set) == 2 and len(is_set) == 1:
            is_set = set(Direction) - cannot_set
        elif len(cannot_set) == 3:
            cannot_set = Direction
        return CellLine(is_set=frozenset(is_set), cannot_set=frozenset(cannot_set))

    def get_through(self):
        """
        Returns the modified CellLine, assuming the line will run straight.
        If there is not enough information to apply this action, the original CellLine is returned.
        If this action is invalid, a `ValueError` exception is thrown.
        Args:
            self
        Returns:
            CellLine: modified CellLine
        """
        num_set = len(self.is_set)
        if num_set == 2:
            one, other = self.is_set
            if one.opposite() != other:
                raise ValueError(f"{self} is already bent!")
            return self
        if num_set == 1:
            one, = self.is_set
            return self.set_direction(one.opposite())
        assert num_set == 0, f"expected none set, found {num_set} ({self.is_set})"

        num_cannot_set = len(self.cannot_set)
        if num_cannot_set == 1:
            one, = self.cannot_set
            cannot_set = frozenset({one, one.opposite()})
            return CellLine(is_set=frozenset(Direction) - cannot_set, cannot_set=cannot_set)
        if num_cannot_set == 2:
            is_set = one, other = frozenset(Direction) - self.cannot_set
            if one.opposite() != other:
                raise ValueError(f"No straight path exists through {self}")
            return CellLine(is_set=is_set, cannot_set=self.cannot_set)
        if num_cannot_set == 4:
            raise ValueError(f"{self} must be blank")

        assert num_cannot_set == 0, (f"expected no `cannot_set`, found {num_cannot_set} "
                                     f"({self.cannot_set})")

        return self

    def get_bent(self):
        """
        Returns the modified CellLine, taking into account that the line will bend.
        If there is insufficient information to apply this action,
        any possible restrictions will be applied.
        If no constraints can be applied, the original CellLine is returned.
        If this action is invalid, a `ValueError` exception is thrown.
        Args:
            self
        Returns:
            CellLine: modified CellLine
        """
        num_set = len(self.is_set)
        if num_set == 2:
            one, other = self.is_set
            if one.opposite() == other:
                raise ValueError(f"{self} is already straight-through!")
            return self
        if num_set == 1:
            one, = self.is_set
            return self.disallow_direction(one.opposite())

        num_cannot_set = len(self.cannot_set)
        if num_cannot_set == 1:
            one, = self.cannot_set
            return self.set_direction(one.opposite())
        if num_cannot_set == 2:
            is_set = one, other = frozenset(Direction) - self.cannot_set
            if one.opposite() == other:
                raise ValueError(f"No bent path exists through {self}")
            return CellLine(is_set=is_set, cannot_set=self.cannot_set)
        if num_cannot_set == 4:
            raise ValueError(f"{self} must be blank")

        assert (num_cannot_set == 0), (f"expected no `cannot_set`, found {num_cannot_set} "
                                       f"({self.cannot_set})")
        return self

    def is_done(self):
        """
        Checks if the cell is already ready (all directions are set or disabled).
        Args:
            self
        Returns:
            bool: done or not
        """
        return len(self.is_set) + len(self.cannot_set) == 4

    def could_set(self):
        """
        Returns an array of possible install destinations.
        Args:
            self
        Returns:
            set
        """
        return frozenset(Direction) - self.is_set - self.cannot_set

    def other_out(self, direction):
        """
        Returns the direction leaving the cell in the given direction (if it exists).
        Args:
            self, direction
        Returns:
            Direction
            None
        """
        other = set(self.is_set - {direction})
        return other.pop() if other else None


@attr.s(frozen=True)
class LineSegment:
    """
    a LineSegment class with attributes to represent a line segment
    """
    start: (int, int) = attr.ib()  # Початкова координата в форматі (x, y)
    start_direction: Direction = attr.ib()  # Напрямок руху від початкової координати
    end: (int, int) = attr.ib()  # Кінцева координата в форматі (x, y)
    end_direction: Direction = attr.ib()  # Напрямок руху до кінцевої координати
    contains: frozenset = attr.ib()  # Множина об'єктів, які проходять через даний відрізок

    def other_end(self, coord):
        """
        Returns other point and direction
        """
        if coord == self.start:
            return self.end, self.end_direction
        return self.start, self.start_direction

    def get(self):
        """
        Get
        """
        return self


def loop_path(coord, direction, cell_lines):
    """
    Moves along the line as far as possible
    and returns the current coordinates and direction of travel.
    Args:
        coord, direction, cell_lines
    Returns:
        None
    """
    x, y = coord
    while direction:
        x, y = direction.move(x, y)  # Переміщуємося в напрямку руху
        direction = direction.opposite()  # Повертаємо напрямок руху назад
        yield (x, y), direction  # Повертаємо поточні координати та напрямок руху
        direction = cell_lines[x, y].other_out(direction)  # Визначаємо наступний напрямок руху


# pylint: disable=too-many-locals
def extend_line_segments(line_segments, cell_lines):
    """
    Returns an updated list of all known line segments with any attachments they have accumulated.
    Args:
        line_segments, cell_lines
    Returns:
        tuple
    """
    # Створюємо словник для швидкого пошуку відрізків за координатами їхніх кінців
    coord_lookup = {coord: seg for seg in line_segments for coord in [seg.start, seg.end]}
    seen_segs = set()  # Множина для відстеження вже оброблених відрізків
    new_segs = []  # Список для зберігання оновлених відрізків
    for segment in line_segments:
        if segment in seen_segs:  # Якщо відрізок вже був оброблений, пропускаємо його
            continue
        seen_segs.add(segment)  # Додаємо відрізок до множини оброблених

        loop = set(segment.contains)  # Початковий набір об'єктів, які проходять через відрізок
        changed_ends = {}  # Словник для зберігання змінених кінців відрізка

        segment_ends = [
            ('start', segment.start, segment.start_direction),
            ('end', segment.end, segment.end_direction),
        ]
        for side, coord, segment_direction in segment_ends:
            cell = cell_lines[coord]  # Отримуємо комірку за координатою
            if len(cell.is_set) == 1:  # Якщо в комірці тільки один об'єкт
                continue  # Пропускаємо цей кінець відрізка
            next_dir = cell.other_out(segment_direction)
            iterator = SwappableIterator(loop_path(coord, next_dir, cell_lines))

            for coord, next_dir in iterator:
                if coord in coord_lookup:  # Якщо знайдено відрізок, що перетинається з поточним
                    merge_seg = coord_lookup[coord]  # Отримуємо відрізок
                    if merge_seg == segment:  # Перевіряємо, чи не утворилася петля
                        raise LoopException(loop)  # Викидаємо виняток
                    seen_segs.add(merge_seg)  # Додаємо відрізок до множини оброблених
                    loop |= merge_seg.contains
                    coord, next_dir = merge_seg.other_end(coord)
                    move_dir = cell_lines[coord].other_out(next_dir)
                    iterator.swap(loop_path(coord, move_dir, cell_lines))
                else:
                    loop.add(coord)
            changed_ends[side] = coord  # Зберігаємо координату кінця відрізка
            changed_ends[f"{side}_direction"] = next_dir

        new_segs.append(attr.evolve(segment,
                                    contains=frozenset(loop),
                                    **changed_ends) if changed_ends else segment)
    return tuple(new_segs)  # Повертаємо оновлені відрізки у вигляді кортежу


def discover_line_segments(cell_lines, seen=()):
    """
    Finds new segments based on `cell_lines`.
    Does not check cells with coordinates that were already in `seen`.
    Args:
        cell_lines, seen
    Returns:
        tuple([])
    """
    seen = set(seen)  # Перетворюємо `seen` у множину для ефективного пошуку
    line_segments = []  # Список для зберігання знайдених відрізків
    for coord, cell in cell_lines.items():  # Перебираємо координати та комірки
        if coord in seen or not cell.is_set:
            continue

        loop = {coord}  # Початковий набір об'єктів, які проходять через поточну комірку
        start = coord
        if len(cell.is_set) == 1:  # Перевірка, чи є в комірці лише один напрямок відрізка.
            [forward_dir] = [back_dir] = cell.is_set
        else:  # Якщо є два напрямки, встановлюємо їх відповідно.
            [forward_dir, back_dir] = cell.is_set
            for start, back_dir in loop_path(coord, back_dir, cell_lines):
                if start in loop:
                    raise LoopException(loop)  # Ми або закінчили, або помиляємось.
                loop.add(start)

        end = coord
        for end, forward_dir in loop_path(coord, forward_dir, cell_lines):
            loop.add(end)

        seen |= loop
        line_segments.append(
            LineSegment(  # Цей відрізок описує шлях від початкової до кінцевої координати
                start=start, start_direction=back_dir,
                end=end, end_direction=forward_dir,
                contains=frozenset(loop),
            )
        )
    return tuple(line_segments)


@attr.s(frozen=True, repr=False)
class Board:
    """
    This class represents a puzzle.
    It has methods for setting lines, checking whether the puzzle is correct,
    and also to find the optimal solution.
    """
    width: int = attr.ib()  # Ширина дошки
    height: int = attr.ib()  # Висота дошки
    circles: {(int, int): bool} = attr.ib()  # Координати кола та її значення
    cell_lines: {(int, int): CellLine} = attr.ib()  # Комірки та інформація про лінії в них
    line_segments: [LineSegment] = attr.ib(cmp=False)  # Відрізки на дошці

    @cell_lines.default
    def _(self):
        return frozendict.frozendict({
            (x, y): CellLine(is_set=frozenset(), cannot_set=self.edges_at(x, y))
            for y in range(self.height)
            for x in range(self.width)
        })

    @line_segments.default
    def _(self):
        return discover_line_segments(self.cell_lines)  # знаходження всіх відрізків.

    def evolve(self, cell_lines):
        """
        Process of solving
        Args:
            self, cell_lines
        Returns:
        """
        cell_lines = frozendict.frozendict(cell_lines)
        try:
            line_segments = extend_line_segments(self.line_segments, cell_lines)
            seen = frozenset().union(*(seg.contains for seg in line_segments))
            line_segments += discover_line_segments(cell_lines, seen)
        except LoopException as exc:
            exc.validate_solved(self.circles)  # Перевіряємо, чи є дошка розв'язаною
            raise SolvedException(
                Board(
                    width=self.width, height=self.height,
                    circles=self.circles, cell_lines=cell_lines,
                    line_segments=[],  # Очищаємо список відрізків, оскільки дошка вирішена
                ),
            )from exc

        return Board(
            width=self.width, height=self.height,
            circles=self.circles, cell_lines=cell_lines,
            line_segments=line_segments,  # Повертаємо оновлену дошку з новим списком відрізків
        )

    def edges_at(self, x, y):
        """
        Returns the set of directions containing the walls on the cell with coordinates (x, y).
        Args:
            self, x, y
        Returns:
            set
        """
        edges = set()
        if x == 0:
            edges.add(Direction.LEFT)
        if y == 0:
            edges.add(Direction.UP)
        if y == self.height - 1:
            edges.add(Direction.DOWN)
        if x == self.width - 1:
            edges.add(Direction.RIGHT)
        return frozenset(edges)

    def set_direction(self, x, y, direction):
        """
        Sets specific direction on cell on x y coordinates.
        Args:
            self, x, y
        Returns:
            Board
        """
        old_cell = self.cell_lines[x, y]
        new_cell = old_cell.set_direction(direction)
        if new_cell == old_cell:
            return self
        return self.propagate_change({(x, y): new_cell})

    def disallow_direction(self, x, y, direction):
        """
        Disallows specific direction on cell on x y coordinates.
        Args:
            self, x, y
        Returns:
            Board
        """
        old_cell = self.cell_lines[x, y]
        new_cell = old_cell.disallow_direction(direction)
        if new_cell == old_cell:
            return self
        return self.propagate_change({(x, y): new_cell})

    def set_through(self, x, y):
        """
        Sets straight line on the cell with coordinates (x, y).
        Args:
            self, x, y
        Returns:
            Board
        """
        old_cell = self.cell_lines[x, y]
        new_cell = old_cell.get_through()
        if new_cell == old_cell:
            return self
        return self.propagate_change({(x, y): new_cell})

    def set_bent(self, x, y):
        """
        Sets the rotation on the cell with coordinates (x, y).
        Args:
            self, x, y
        Returns:
            Board
        """
        old_cell = self.cell_lines[x, y]
        new_cell = old_cell.get_bent()
        if new_cell == old_cell:
            return self
        return self.propagate_change({(x, y): new_cell})

    def propagate_change(self, changes):
        """
        Propagate changes in the state of cells that have been changed.
        Args:
            self, changes
        Returns:
            Board
        """
        # Створюємо пошукову мапу для комірок, в якій спочатку шукаємо внесені зміни
        cell_lookup = collections.ChainMap(changes, self.cell_lines)

        positions = collections.deque(changes)
        while positions:
            x, y = positions.popleft()
            for direction in changes[x, y].is_set:
                mx, my = direction.move(x, y)
                old_cell = cell_lookup[mx, my]
                new_cell = old_cell.set_direction(direction.opposite())
                if new_cell == old_cell:
                    continue
                positions.append((mx, my))
                changes[mx, my] = new_cell

            for direction in changes[x, y].cannot_set:
                mx, my = direction.move(x, y)
                old_cell = cell_lookup.get((mx, my))
                if not old_cell:  # Якщо нової комірки не існує, переходимо до наступного напрямку
                    continue
                new_cell = old_cell.disallow_direction(direction.opposite())
                if new_cell == old_cell:
                    continue
                positions.append((mx, my))
                changes[mx, my] = new_cell
        return self.evolve(cell_lines=cell_lookup)

    # Повертає рядок, який представляє об'єкт дошки.
    def __repr__(self):
        return "Board"


class SwappableIterator:
    """
    Iterator
    """
    def __init__(self, iterator):
        self._iterator = iter(iterator)

    def __iter__(self):
        return self

    def __next__(self):
        return next(self._iterator)

    def swap(self, iterable):
        """
        Swaps iterators
        Args:
            self, iterable
        Returns:
            None
        """
        self._iterator = iter(iterable)


def apply_white(board, x, y):
    """
    Ця функція відповідає за обробку логіки при застосуванні білого кружка на дошці.
    Вона встановлює кружок, щоб дозволити лініям проходити через нього і спробує вигнути лінії,
    які прилягають до білого кружка відповідно до певних умов.
    Args:
        board, x, y
    Returns:
        Board: new board
    """
    board = board.set_through(x, y)
    cell_set = board.cell_lines[x, y].is_set
    if len(cell_set) != 2:  # Якщо напрямків не рівно два, повертаємо діючу дошку.
        return board

    left, right = cell_set
    lx, ly = left.move(x, y)
    try:
        bend_left = board.set_bent(lx, ly)
        left_must_straight = False
    except ValueError:
        left_must_straight = True

    rx, ry = right.move(x, y)
    try:
        bend_right = board.set_bent(rx, ry)
        right_must_straight = False
    except ValueError:
        right_must_straight = True

    if left_must_straight and right_must_straight:
        raise ValueError(f"Cannot bend either end of the white circle at {x}, {y}")

    if not left_must_straight and not right_must_straight:
        return board

    assert bool(left_must_straight) != bool(right_must_straight), "Expected exactly one may bend"
    return bend_right if left_must_straight else bend_left


def apply_black(board, x, y):
    """
    This function is responsible  applying the black circle on the board.
    It sets the circle and also curves the line segment at that point.
    It then extends the existing lines through the center cell,
    adds new lines in directions where possible and returns the changed board.
    Args:
        board, x, y
    Returns:
        Board: new board
    """
    board = board.set_bent(x, y)
    cell = board.cell_lines[x, y]

    for direction in cell.is_set:
        mx, my = direction.move(x, y)
        board = board.set_through(mx, my)

    if cell.is_done():
        return board

    could_dirs = set()
    for direction in cell.could_set():
        with contextlib.suppress(ValueError):
            set_black_leg(board, x, y, direction)
            could_dirs.add(direction)

    for direction in could_dirs:
        if direction.opposite() not in could_dirs:
            board = set_black_leg(board, x, y, direction)
    return board


def set_black_leg(board, x, y, direction):
    """
    sets the direction for the black circle to position (x, y)
    and passes through the cell where the black circle is located.
    Args:
        board, x, y, direction
    Returns:
        Board: new board
    """
    mx, my = direction.move(x, y)  # Встановлюємо напрямок для чорного кружка на позиції (x, y).
    board = board.set_direction(x, y, direction)
    return board.set_through(mx, my)  # Проходимо через комірку, де розташований чорний кружок.


def solve_known_constraints(board):
    """
    Resolves known blackboard limitations by applying the apply_white and apply_black functions
    to each circle until a state is reached where no more changes can be made to the board.
    Args:
        board(Board): board
    Returns:
        Board: new board
    """
    old_board = None
    while old_board != board:
        old_board = board
        for (x, y), circle in board.circles.items():
            if circle == 'W':
                board = apply_white(board, x, y)
            else:
                board = apply_black(board, x, y)
    return board


def spot_direction_options(board, x, y, direction):
    """
    Determines the possible directions for the given cell (x, y) and direction.
    Returns the resolved constraints for the cell that points to the specified direction,
    and also for a cell that prohibits the specified direction.
    Args:
        board, x, y, direction
    Returns:
    """
    with contextlib.suppress(ValueError):
        yield solve_known_constraints(board.set_direction(x, y, direction))
    with contextlib.suppress(ValueError):
        yield solve_known_constraints(board.disallow_direction(x, y, direction))


UNEXPLORED = sentinel.UNEXPLORED


def get_possibility_list(lookahead):
    """
    Ця функція використовується для генерації списку можливих наступних кроків
    або одного можливого наступного стану дошки в алгоритмі lookahead.
    Вона перебирає всі комірки дошки та їх можливі напрямки,
    обираючи лише напрямки, які визначені маскою (у цьому випадку напрямки праворуч та вниз).
    Args:
        lookahead(Lookahead)
    Returns:
        list: list of possibilities
    """
    possibilities = []  # Список можливостей для наступного кроку
    mask = {Direction.RIGHT, Direction.DOWN}  # Маска напрямків для розгляду

    board = lookahead.board  # Отримання дошки з атрибуту lookahead
    for (x, y), cell in board.cell_lines.items():
        for direction in cell.could_set() & mask:  # Перебір усіх комірок та їх можливих напрямків
            boards = list(spot_direction_options(board, x, y, direction))
            if len(boards) == 1:  # Якщо є тільки один наступний стан
                next_board, = boards
                return next_board
            if not boards:  # Якщо немає наступних станів
                return []
            possibilities.append(PossibilityPair.new(*boards, parent=lookahead))
    return possibilities  # Повернення списку можливостей


class Ref:
    """
    Ref
    """

    def __init__(self, value):
        self.__ref__ = value

    def __getattr__(self, key):
        return getattr(self.__ref__, key)

    def __setattr__(self, key, value):
        if key == '__ref__':
            object.__setattr__(self, key, value)
        else:
            setattr(self.__ref__, key, value)

    def __repr__(self):
        return f"Ref({self.__ref__})"


@attr.s
class Lookahead:
    """
    is used to store information about some state of the game and possible next steps
    """
    board = attr.ib()  # поточний стан гри
    possibilities = attr.ib()  # Поле, яке зберігає можливі наступні кроки, які можна виконати в грі
    parent = attr.ib()  # зберігає батьківський елемент

    @classmethod
    def new(cls, board, parent=None):
        """
        creates a new instance of the Lookahead class.
        Used to create new objects, where Parent points to the Parent object.
        If parent is already specified,
        it is stored as a weak reference to avoid circular dependency.
        Args:
            cls, board, parent
        Returns:
            Lookahead: new
        """
        if parent is not None:
            parent = weakref.ref(parent)
        return cls(board, possibilities=UNEXPLORED, parent=parent)

    def get_sibling(self):
        """
        Function returns a sibling object to the current one.
        It can be another object of the Lookahead class,
        that shares a parent with the current object.
        Used to navigate the game's feature tree.
        Args:
            self(Lookahead)
        Returns:
            Lookahead: sibling
        """
        pos = self.parent()
        assert pos
        assert self in (pos.yes.__ref__, pos.no.__ref__)
        return pos.no if pos.yes.__ref__ == self else pos.yes


def explore(lookahead_ref):
    """
    Function used to expand the possibilities of the game by exploring different options for moves.
    Args:
        lookahead_ref(Lookahead)
    Returns:
        bool: expanded or not
    """
    queue = collections.deque([lookahead_ref])  # Створення черги для обробки об'єктів Lookahead
    while queue:  # Поки черга не порожня
        lookahead = queue.popleft()  # Вибираємо наступний об'єкт Lookahead з черги
        if lookahead.possibilities is UNEXPLORED:  # Якщо можливості ще не досліджені
            expand(lookahead)  # здійснення нових ходів для поточного об'єкта Lookahead
            return True
        for pos in lookahead.possibilities:
            queue.append(pos.yes)
            queue.append(pos.no)
    return False


def expand(lookahead_ref):
    """
    Function used to extend the capabilities of the Lookahead object.
    Args:
        lookahead_ref(Lookahead)
    Returns:
        None
    """
    possibilities = get_possibility_list(lookahead_ref)
    if not possibilities:  # Якщо немає можливостей
        if lookahead_ref.parent is None:  # Суперечність
            raise ValueError("root lookahead encountered contradiction")
        sibling = lookahead_ref.get_sibling()
        parent = lookahead_ref.parent().parent()
        assert parent, "parent was gc'd??"
        sibling.parent = parent.parent
        parent.__ref__ = sibling.__ref__
        if sibling.possibilities is not UNEXPLORED:
            for pos in sibling.possibilities:
                pos.parent = weakref.ref(parent)

    elif isinstance(possibilities, list):  # Якщо є списком можливостей
        lookahead_ref.possibilities = possibilities
        assert 'possibilities' not in lookahead_ref.__dict__
    else:  # Якщо є визначеною дошкою
        assert isinstance(possibilities, Board)
        lookahead_ref.board = possibilities


@attr.s
class PossibilityPair:
    """
    Presents a couple of possible game development options for the player.
    """
    yes = attr.ib()  # yes і no - можливі варіанти, які може прийняти гравець
    no = attr.ib()
    parent = attr.ib()

    @classmethod
    def new(cls, yes_board, no_board, *, parent):
        """
        Creating a new instance of the PossibilityPair class.
        Args:
            cls, yes_board, no_board, *, parent
        Returns:
            PossibilityPair: possibility pair.
        """
        self = cls(None, None, parent=weakref.ref(parent))
        self.yes = Ref(Lookahead.new(yes_board, parent=self))
        self.no = Ref(Lookahead.new(no_board, parent=self))
        return self

    def get(self):
        """
        Get
        """
        return self


def solve(board):
    """
    Function to run solving process.
    Args:
        board(Board): input board
    Returns:
        Board: solved board.
        None
    """
    try:
        # Створюємо корінь дерева можливостей
        root = Ref(Lookahead.new(solve_known_constraints(board)))
        last_seen_board = root.board  # Зберігаємо останню бачену дошку
        while explore(root):  # Продовжуємо досліджувати граф можливостей
            # Виводимо дошку, якщо отримали нову інформацію
            if last_seen_board is not root.board:
                print(print_big_board(root.board))
                last_seen_board = root.board
    except SolvedException as exc:
        # Якщо гра вирішена, повертаємо вирішену дошку
        return exc.board
    return None


def board_from_string(input_str):
    """
    Function to create board from string.
    Args:
        input_str(str): input string
    Returns:
        Board: board.
    """
    input_str = input_str.replace("\\n", "\n")
    board_lines = textwrap.dedent(input_str.rstrip().strip("\n")).split("\n")
    print("Creating board from:")
    for line in board_lines:
        print(line)

    return Board(
        width=len(board_lines[0]),
        height=len(board_lines),
        circles=frozendict.frozendict({
            (x, y): elem
            for y, row in enumerate(board_lines)
            for x, elem in enumerate(row)
            if elem != '.'
        }),
    )


INNER_CELL_LINE = {
    frozenset({Direction.DOWN, Direction.UP}): '│',
    frozenset({Direction.LEFT, Direction.RIGHT}): '─',
    frozenset({Direction.LEFT, Direction.DOWN}): '┐',
    frozenset({Direction.LEFT, Direction.UP}): '┘',
    frozenset({Direction.RIGHT, Direction.UP}): '└',
    frozenset({Direction.RIGHT, Direction.DOWN}): '┌',
}


def print_big_board(board):
    """
    Function to generate board interface.
    Args:
        board(Board): input board
    Returns:
        str: board display.
    """
    board_lines = ['┌', '┬'.join('─' * board.width), '┐\n']  # Перша лінія таблиці

    for row in range(board.height):
        board_lines.extend(['│'])  # Перший стовпець знаків |
        for col in range(board.width):
            cell = board.cell_lines[col, row]
            if (col, row) in board.circles:  # Якщо в цій комірці є коло
                board_lines.append(board.circles[col, row])  # Додаємо коло
            else:
                # Додаємо символ відповідно до напрямків ліній
                board_lines.append(INNER_CELL_LINE.get(cell.is_set, ' '))
            if Direction.RIGHT in cell.is_set:
                board_lines.append('─')  # Горизонтальна лінія вправо
            else:
                board_lines.extend(['│'])  # Вертикальна лінія зліва

        if row == board.height - 1:
            board_lines.extend(['\n└', '┴'.join('─' * board.width), '┘'])  # Остання лінія таблиці
        else:
            board_lines.extend(['\n├'])
            for col in range(board.width):
                cell = board.cell_lines[col, row]
                if Direction.DOWN in cell.is_set:
                    board_lines.extend(['│'])  # Вертикальна лінія вниз
                else:
                    board_lines.append('─')  # Горизонтальна лінія

                board_lines.append('┤' if col == board.width - 1 else '┼')  # Розділювач в ячейці

        board_lines.append('\n')
    return ''.join(board_lines)  # Повертаємо сформований текст таблиці


def main():
    """
    Function main to run program.
    Returns:
        void
    """
    board_str = ".W....\n" + "...B..\n" + "W.W...\n" + ".....W\n" + "..B...\n" + ".....B\n"
    board = board_from_string(board_str)
    print(print_big_board(board))
    solved_board = solve(board)
    if solved_board:
        print(print_big_board(solved_board))


if __name__ == '__main__':
    main()
