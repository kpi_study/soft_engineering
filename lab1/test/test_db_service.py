"""
Модуль тестів для роботи з бд
"""
from unittest.mock import MagicMock
from unittest.mock import call
import pytest
import pymysql
import db_service as db


# pylint: disable=redefined-outer-name
@pytest.fixture
def db_connection(mocker):
    """
    Фікстура для підключення до бази даних.
    Ця фікстура буде ініціалізувати з'єднання з базою даних перед
    кожним тестом і закривати його після завершення тесту.
    Args:
        mocker
    Returns:
    """
    conn = db.get_connection()
    cursor_mock = MagicMock()
    conn.cursor = mocker.MagicMock(return_value=cursor_mock)
    yield conn


def test_get_connection_successful():
    """
    Тест для функції get_connection() без мокування, просто перевірка успішності підключення
    Args:
    Returns:
        bool
    """
    connection = db.get_connection()
    assert connection is not None


def test_get_connection_error(mocker, capfd):
    """
    Тест для функції get_connection() з мокуванням в разі виникнення помилки підключення
    Args:
        mocker
        capfd
    Returns:
        bool
    """
    # Моки для констант підключення
    mocker.patch('db_service.HOST', 'invalid_host')
    mocker.patch('db_service.USER', 'invalid_user')
    mocker.patch('db_service.PASSWORD', 'invalid_password')
    mocker.patch('db_service.DB_NAME', 'invalid_db')

    # Мок для pymysql.connect, який викидає виняток
    connect_mock = mocker.patch('db_service.pymysql.connect')
    connect_mock.side_effect = pymysql.Error('Connection error')

    connection = db.get_connection()

    assert connection is None
    captured = capfd.readouterr()
    assert '- DB: Connection error!' in captured.out
    assert 'Connection error' in captured.out


def test_insert_problem(db_connection, mocker):
    """
    Тест для функції додавання нової умови
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    commit_mock = mocker.patch.object(db_connection, 'commit')
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)
    problem_str = "Some problem string"

    result = db.insert_problem(db_connection, problem_str)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "INSERT INTO `masyu_problems` (problem) VALUES ('" + problem_str + "');"
    )
    commit_mock.assert_called_once()
    assert result is True


def test_insert_problem_exception(db_connection, mocker):
    """
    Тест для функції додавання нової умови при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    problem_str = "Some problem string"
    result = db.insert_problem(db_connection, problem_str)

    assert result is False


def test_insert_solving_to_existing_problem_id(db_connection, mocker):
    """
    Тест для функції додавання рішення до умови по ід
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    commit_mock = mocker.patch.object(db_connection, 'commit')
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)
    solving_str = "Some solving string"

    result = db.insert_solving_to_existing_problem_id(db_connection, 1, solving_str)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "INSERT INTO `masyu_solvings` (problem_id, solving) "
        "VALUES ('" + str(1) + "', '" + solving_str + "');"
    )
    commit_mock.assert_called_once()
    assert result is True


def test_insert_solving_to_existing_problem_id_exception(db_connection, mocker):
    """
    Тест для функції додавання рішення до ід умови з помилкою
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    solving_str = "Some solving string"

    result = db.insert_solving_to_existing_problem_id(db_connection, 1,  solving_str)

    assert result is False


def test_insert_solving_to_existing_problem(db_connection, mocker):
    """
    Тест для функції додавання рішення до умови
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    commit_mock = mocker.patch.object(db_connection, 'commit')
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    cursor_mock.__enter__.return_value.fetchone.return_value = {'id': 1}

    solving_str = "Some solving string"
    problem_str = "Some problem string"

    result = db.insert_solving_to_existing_problem(db_connection, problem_str, solving_str)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_has_calls([
        call("SELECT id FROM `masyu_problems` WHERE problem='" + problem_str + "';"),
        call("INSERT INTO `masyu_solvings` (problem_id, solving) "
             "VALUES ('1', '" + solving_str + "');")
    ])
    commit_mock.assert_called_once()
    assert result is True


def test_insert_solving_to_existing_problem_exception(db_connection, mocker):
    """
    Тест для функції додавання рішення до умови з помилкою
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    solving_str = "Some solving string"
    problem_str = "Some problem string"

    result = db.insert_solving_to_existing_problem(db_connection, problem_str,  solving_str)

    assert result is False


def test_add_user(db_connection, mocker):
    """
    Тест для функції додавання нового користувача
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    commit_mock = mocker.patch.object(db_connection, 'commit')
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    result = db.add_user(db_connection, "name", "pass", "key")

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "INSERT INTO users (username, hashed_password, developer_key) "
        "VALUES ('name', 'pass', 'key')"
    )
    commit_mock.assert_called_once()
    assert result is True


def test_add_user_exception(db_connection, mocker):
    """
    Тест для функції додавання нового користувача при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.add_user(db_connection, "name", "pass", "key")

    assert result is False


def test_select_problem_by_id_found(db_connection, mocker):
    """
    Тест для функції вибору умови по ід
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)
    expected_problem_string = "Test Problem"

    cursor_mock.__enter__.return_value.fetchone.return_value = {'problem': expected_problem_string}

    result = db.select_problem_by_id(db_connection, 1)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT problem FROM `masyu_problems` WHERE id=1;"
    )
    assert result == expected_problem_string


def test_select_problem_by_id_not_found(db_connection, mocker):
    """
    Тест для функції вибору умови по ід коли вона не знайдена
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking the database response for no result found
    cursor_mock.__enter__.return_value.fetchone.return_value = None

    result = db.select_problem_by_id(db_connection, 2)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT problem FROM `masyu_problems` WHERE id=2;"
    )
    assert result == ""


def test_select_problem_by_id_exception(db_connection, mocker):
    """
    Тест для функції вибору умови по ід при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking cursor.execute to raise an exception
    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.select_problem_by_id(db_connection, 3)

    assert result == ""


def test_select_random_problem_found(db_connection, mocker):
    """
    Тест для функції вибору випадкової умови
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)
    expected_problem_string = "Test Problem"

    cursor_mock.__enter__.return_value.fetchone.return_value = {'problem': expected_problem_string}

    result = db.select_random_problem(db_connection)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT * FROM `masyu_problems` ORDER BY RAND() LIMIT 1;"
    )
    assert result == expected_problem_string


def test_select_random_problem_not_found(db_connection, mocker):
    """
    Тест для функції вибору випадкової умови якщо вона не знайдена
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking the database response for no result found
    cursor_mock.__enter__.return_value.fetchone.return_value = None

    result = db.select_random_problem(db_connection)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT * FROM `masyu_problems` ORDER BY RAND() LIMIT 1;"
    )
    assert result == ""


def test_select_random_problem_exception(db_connection, mocker):
    """
    Тест для функції вибору випадкової умови при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking cursor.execute to raise an exception
    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.select_random_problem(db_connection)

    assert result == ""


def test_select_solving_by_existing_problem_id(db_connection, mocker):
    """
    Тест для функції вибору рішень по ід умови
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)
    expected_problem_string = "Test Problem"

    expected = cursor_mock.__enter__.return_value.fetchall.return_value = {
        'problem': expected_problem_string}

    result = db.select_solving_by_existing_problem_id(db_connection, 1)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT distinct solving FROM `masyu_solvings` WHERE problem_id=1;"
    )
    assert result == expected


def test_select_solving_by_existing_problem_id_not_found(db_connection, mocker):
    """
    Тест для функції вибору рішень по ід умови, коли таких не знайдено
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking the database response for no result found
    cursor_mock.__enter__.return_value.fetchall.return_value = ""

    result = db.select_solving_by_existing_problem_id(db_connection, 2)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT distinct solving FROM `masyu_solvings` WHERE problem_id=2;"
    )
    assert result == ""


def test_select_solving_by_existing_problem_id_exception(db_connection, mocker):
    """
    Тест для функції вибору рішень по ід умови при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking cursor.execute to raise an exception
    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.select_solving_by_existing_problem_id(db_connection, 3)

    assert result == ""


def test_select_solving_by_existing_problem(db_connection, mocker):
    """
    Тест для функції вибору рішень по умові
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)
    expected_solving_string = "Test Solving"
    problem_str = "Some problem string"

    expected = cursor_mock.__enter__.return_value.fetchall.return_value = {
        'problem': expected_solving_string}

    result = db.select_solving_by_existing_problem(db_connection, problem_str)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT distinct solving FROM `masyu_solvings` "
        "JOIN `masyu_problems` ON masyu_solvings.problem_id=masyu_problems.id "
        "WHERE problem='" + problem_str + "';"
    )
    assert result == expected


def test_select_solving_by_existing_problem_not_found(db_connection, mocker):
    """
    Тест для функції вибору рішень по умові коли такі не знйдені
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking the database response for no result found
    cursor_mock.__enter__.return_value.fetchall.return_value = ""

    result = db.select_solving_by_existing_problem(db_connection, "Problem string")

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT distinct solving FROM `masyu_solvings` "
        "JOIN `masyu_problems` ON masyu_solvings.problem_id=masyu_problems.id "
        "WHERE problem='Problem string';"
    )
    assert result == ""


def test_select_solving_by_existing_problem_exception(db_connection, mocker):
    """
    Тест для функції вибору рішень по умові при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    # Mocking cursor.execute to raise an exception
    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.select_solving_by_existing_problem(db_connection, "Problem string")

    assert result == ""


def test_check_user_exists(db_connection, mocker):
    """
    Тест для функції перевірки наявності юзера в бд
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    result = db.check_user_exists(db_connection, "name")

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT * FROM users WHERE username = 'name';"
    )
    assert result is True


def test_check_user_exists_exception(db_connection, mocker):
    """
    Тест для функції перевірки наявності юзера в бд при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.check_user_exists(db_connection, "name")

    assert result is False


def test_get_user_by_username(db_connection, mocker):
    """
    Тест для функції вибору юзера за іменем
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    expected = cursor_mock.__enter__.return_value.fetchone.\
        return_value = {'id': 1, 'username': 'name',
                        "hashed_password": 'pass', 'developer_key': 'key'}

    result = db.get_user_by_username(db_connection, "name")

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT * FROM users WHERE username = 'name';"
    )
    assert result == expected


def test_get_user_by_username_not_found(db_connection, mocker):
    """
    Тест для функції вибору юзера за іменем, коли воно не знайдене
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    cursor_mock.__enter__.return_value.fetchone.return_value = None

    result = db.get_user_by_username(db_connection, "name")

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT * FROM users WHERE username = 'name';"
    )
    assert result is None


def test_get_user_by_username_exception(db_connection, mocker):
    """
    Тест для функції вибору юзера за іменем при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.get_user_by_username(db_connection, "name")

    assert result is None


def test_get_all_users(db_connection, mocker):
    """
    Тест для функції вибору усіх юзерів
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    expected = cursor_mock.__enter__.return_value.fetchall.\
        return_value = {'id': 1, 'username': 'name',
                        "hashed_password": 'pass', 'developer_key': 'key'}

    result = db.get_all_users(db_connection)

    db_connection.cursor.assert_called_once()
    cursor_mock.__enter__.return_value.execute.assert_called_once_with(
        "SELECT * FROM users"
    )
    assert result == expected


def test_get_all_users_exception(db_connection, mocker):
    """
    Тест для функції вибору усіх юзерів при помилці
    Args:
        db_connection
        mocker
    Returns:
        bool
    """
    cursor_mock = mocker.MagicMock()
    mocker.patch.object(db_connection, 'cursor', return_value=cursor_mock)

    execute_mock = cursor_mock.__enter__.return_value.execute
    execute_mock.side_effect = pymysql.Error("Test exception")

    result = db.get_all_users(db_connection)

    assert result == ""
