"""
Модуль з тестами вирішення головоломки.
"""
import contextlib
from unittest.mock import MagicMock
import pytest

from task31 import (
    Board, Direction, CellLine, LineSegment, LoopException, SolvedException,
    board_from_string, discover_line_segments, spot_direction_options,
    extend_line_segments, apply_white, apply_black, set_black_leg,
    solve_known_constraints, solve, print_big_board,
    Lookahead, Ref, main, PossibilityPair
)

BOARD_STR = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B"


def test_direction_methods():
    """
    Function to test direction methods.

    Args:

    Returns:
        bool: correct or not.
    """
    assert Direction.UP.opposite() == Direction.DOWN
    assert Direction.DOWN.opposite() == Direction.UP
    assert Direction.LEFT.opposite() == Direction.RIGHT
    assert Direction.RIGHT.opposite() == Direction.LEFT

    assert Direction.UP.turn_right() == Direction.RIGHT
    assert Direction.RIGHT.turn_right() == Direction.DOWN
    assert Direction.DOWN.turn_right() == Direction.LEFT
    assert Direction.LEFT.turn_right() == Direction.UP

    assert Direction.UP.turn_left() == Direction.LEFT
    assert Direction.LEFT.turn_left() == Direction.DOWN
    assert Direction.DOWN.turn_left() == Direction.RIGHT
    assert Direction.RIGHT.turn_left() == Direction.UP


def test_board_direction_methods():
    """
    Function to test board direction methods.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)

    board = board.set_direction(1, 0, Direction.RIGHT)
    board_new = board.set_direction(1, 0, Direction.RIGHT)
    assert board == board_new

    board = board.disallow_direction(1, 0, Direction.DOWN)
    assert board != board_new
    board_new = board.disallow_direction(1, 0, Direction.DOWN)
    assert board == board_new

    # Очікуване рядкове представлення
    expected_repr = "Board"
    # Отримуємо рядкове представлення за допомогою методу __repr__()
    actual_repr = repr(board)
    # Порівнюємо отримане рядкове представлення з очікуваним
    assert actual_repr == expected_repr


def test_cellline_methods():
    """
    Function to test cellline methods.

    Args:

    Returns:
        bool: correct or not.
    """
    cellline = CellLine(frozenset(), frozenset())
    assert cellline.is_done() is False
    assert cellline.could_set() == set(Direction)

    cellline = cellline.set_direction(Direction.UP)
    assert cellline.is_set == {Direction.UP}
    assert cellline.could_set() == {Direction.LEFT, Direction.RIGHT, Direction.DOWN}

    cellline = cellline.set_direction(Direction.RIGHT)
    assert cellline.is_set == {Direction.UP, Direction.RIGHT}
    assert cellline.cannot_set == frozenset({Direction.DOWN, Direction.LEFT})
    assert cellline.could_set() == set()
    assert cellline.is_done() is True

    with pytest.raises(ValueError):
        cellline.set_direction(Direction.DOWN)

    with pytest.raises(ValueError):
        cellline.disallow_direction(Direction.UP)

    cellline = CellLine(frozenset(), frozenset())
    cellline = cellline.disallow_direction(Direction.UP)
    assert cellline.cannot_set == frozenset({Direction.UP})


def test_get_through():
    """
    Function to test get through method.

    Args:

    Returns:
        bool: correct or not.
    """
    cell_line = CellLine(is_set={Direction.UP, Direction.DOWN}, cannot_set=set())

    modified_cell_line = cell_line.get_through()

    assert modified_cell_line.is_set == frozenset({Direction.UP, Direction.DOWN})
    assert modified_cell_line.cannot_set == set()

    # bent line
    cell_line = CellLine(is_set={Direction.UP, Direction.LEFT}, cannot_set=set())

    with pytest.raises(ValueError):
        cell_line.get_through()


def test_get_bent():
    """
    Function to test get bent method.

    Args:

    Returns:
        bool: correct or not.
    """
    # bent line
    cell_line = CellLine(is_set={Direction.UP, Direction.LEFT}, cannot_set=set())
    modified_cell_line = cell_line.get_bent()

    assert modified_cell_line.is_set == frozenset({Direction.UP, Direction.LEFT})
    assert modified_cell_line.cannot_set == set()

    # through line
    cell_line = CellLine(is_set={Direction.UP, Direction.DOWN}, cannot_set=set())
    with pytest.raises(ValueError):
        cell_line.get_bent()

    cell_line = CellLine(is_set=set(), cannot_set={Direction.UP, Direction.DOWN})
    with pytest.raises(ValueError):
        cell_line.get_bent()

    cell_line = CellLine(is_set=set(),
                         cannot_set={Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT})
    with pytest.raises(ValueError):
        cell_line.get_bent()


def test_linesegment_methods():
    """
    Function to test linesegment methods.

    Args:

    Returns:
        bool: correct or not.
    """
    segment = LineSegment((0, 0), Direction.RIGHT,
                          (1, 0), Direction.LEFT,
                          frozenset({(0, 0), (1, 0)}))
    assert segment.other_end((0, 0)) == ((1, 0), Direction.LEFT)
    assert segment.other_end((1, 0)) == ((0, 0), Direction.RIGHT)
    assert segment.get() == LineSegment((0, 0), Direction.RIGHT,
                                        (1, 0), Direction.LEFT,
                                        frozenset({(0, 0), (1, 0)}))


def test_loop_exception():
    """
    Function to test loop exception.

    Args:

    Returns:
        bool: correct or not.
    """
    loop_coords = {(0, 0), (1, 0), (2, 0)}
    exc = LoopException(loop_coords)
    with pytest.raises(ValueError, match="Closed loop does not contain all circles"):
        exc.validate_solved({(0, 0), (1, 0), (2, 0), (3, 0)})


def test_solved_exception():
    """
    Function to test loop exception.

    Args:

    Returns:
        bool: correct or not.
    """
    # Підготовка даних для тесту
    board = board_from_string(BOARD_STR)

    # Перевірка, що виникає виключення SolvedException з правильною дошкою
    with pytest.raises(SolvedException) as exc_info:
        raise SolvedException(board)

    # Перевірка, що об'єкт виключення містить правильну дошку
    assert exc_info.value.board == board


def test_board_from_string():
    """
    Function to test board from string generating method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)
    assert board.width == 6
    assert board.height == 6
    assert board.circles == {(1, 0): 'W',
                             (3, 1): 'B',
                             (0, 2): 'W',
                             (2, 2): 'W',
                             (5, 3): 'W',
                             (2, 4): 'B',
                             (5, 5): 'B'}


def test_discover_line_segments():
    """
    Function to test discover line segment method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)

    board = board.set_direction(1, 0, Direction.RIGHT)
    board = board.set_direction(1, 0, Direction.LEFT)
    print(board.cell_lines[(1, 0)])

    segments = discover_line_segments(board.cell_lines)
    print(print_big_board(board))
    print(segments)
    assert len(segments) == 1
    assert (LineSegment((2, 0), Direction.LEFT, (0, 1), Direction.UP,
                        frozenset({(0, 1), (1, 0), (2, 0), (0, 0)})) in segments
            or LineSegment((0, 1), Direction.UP, (2, 0), Direction.LEFT,
                           frozenset({(0, 1), (1, 0), (2, 0), (0, 0)})) in segments)


def test_extend_line_segments():
    """
    Function to test extend line segments method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)
    board = board.set_direction(1, 0, Direction.RIGHT)
    board = board.set_direction(1, 0, Direction.LEFT)

    segments = discover_line_segments(board.cell_lines)
    extended_segments = extend_line_segments(segments, board.cell_lines)
    print(print_big_board(board))
    assert len(extended_segments) == 1
    assert (LineSegment((2, 0), Direction.LEFT, (0, 1), Direction.UP,
                        frozenset({(1, 0), (0, 1), (2, 0), (0, 0)})) in extended_segments
            or LineSegment((0, 1), Direction.UP, (2, 0), Direction.LEFT,
                           frozenset({(0, 1), (1, 0), (2, 0), (0, 0)})) in extended_segments)


def test_apply_white():
    """
    Function to test applying white rule methods.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)
    board = apply_white(board, 1, 0)
    assert board.cell_lines[(1, 0)].is_set == frozenset({Direction.RIGHT, Direction.LEFT})

    board = apply_white(board, 5, 3)
    assert board.cell_lines[(5, 3)].is_set == frozenset({Direction.UP, Direction.DOWN})


def test_apply_black():
    """
    Function to test applying black rule method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)
    board = apply_black(board, 3, 1)
    assert board.cell_lines[(3, 1)].is_set == frozenset({Direction.DOWN})
    assert board.cell_lines[(3, 0)].is_set == frozenset()
    assert board.cell_lines[(3, 2)].is_set == frozenset({Direction.UP, Direction.DOWN})
    assert board.cell_lines[(2, 1)].is_set == frozenset()
    assert board.cell_lines[(4, 1)].is_set == frozenset()


def test_set_black_leg():
    """
    Function to test set black leg method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)
    with contextlib.suppress(ValueError):
        board = set_black_leg(board, 3, 1, Direction.RIGHT)
    assert board.cell_lines[(3, 1)].is_set == frozenset({Direction.RIGHT})


def test_solve_known_constraints():
    """
    Function to test solving known constraints method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)
    solved_board = solve_known_constraints(board)
    assert solved_board.cell_lines[(1, 0)].is_set == frozenset({Direction.LEFT, Direction.RIGHT})
    assert solved_board.cell_lines[(3, 1)].is_set == frozenset({Direction.DOWN, Direction.RIGHT})
    assert solved_board.cell_lines[(0, 2)].is_set == frozenset({Direction.UP, Direction.DOWN})
    assert solved_board.cell_lines[(2, 2)].is_set == frozenset({Direction.UP, Direction.DOWN})
    assert solved_board.cell_lines[(5, 3)].is_set == frozenset({Direction.UP, Direction.DOWN})
    assert solved_board.cell_lines[(2, 4)].is_set == frozenset({Direction.LEFT, Direction.UP})
    assert solved_board.cell_lines[(5, 5)].is_set == frozenset({Direction.UP, Direction.LEFT})

    with contextlib.suppress(ValueError):
        result_known = list(spot_direction_options(board, 1, 0, Direction.DOWN))
        assert result_known

    with contextlib.suppress(ValueError):
        result_disallow = list(spot_direction_options(board, 1, 0, Direction.UP))
        assert result_disallow


def test_ref():
    """
    Function to test ref class.

    Args:

    Returns:
        bool: correct or not.
    """
    obj = MagicMock()
    ref = Ref(obj)
    ref.attr = 42
    assert obj.attr == 42


def test_lookahead_new():
    """
    Function to test lookahead new method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = Board(width=3, height=3, circles={})
    parent = Lookahead(board, [], None)
    child = Lookahead.new(board, parent)
    assert child.parent() == parent


def test_possibility_pair_new():
    """
    Function to test possibility pair new method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = Board(width=3, height=3, circles={})
    board2 = Board(width=3, height=3, circles={})
    parent = Lookahead(board, [], None)
    pair = PossibilityPair.new(board, board2, parent=parent)
    assert pair.yes.board == board
    assert pair.no.board == board2


def test_solve():
    """
    Function to test solving method.

    Args:

    Returns:
        bool: correct or not.
    """
    board = board_from_string(BOARD_STR)
    solved_board = solve(board)
    assert isinstance(solved_board, Board)
    assert len(solved_board.line_segments) == 0
    for coord, cell_line in solved_board.cell_lines.items():
        assert cell_line.is_done()
        assert coord
    test_solve_known_constraints()


def test_main():
    """
    Function to test main method.

    Args:

    Returns:
        bool: correct or not.
    """
    # Викликаємо функцію main і перевіряємо, що вона не повертає помилку
    assert main() is None
