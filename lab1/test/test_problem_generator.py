import pytest
from problem_generator import generate_random_masyu, is_solvable, generate_and_test
# coverage run -m pytest test_main.py test_task31.py
# coverage report


def test_generate_random_masyu():
    """
    Checks that the generated puzzle has the correct dimensions
    and the specified number of white and black circles
    :return: bool
    """
    width, height = 6, 6
    num_white, num_black = 3, 3
    puzzle = generate_random_masyu(width, height, num_white, num_black)
    assert puzzle.count('W') == num_white
    assert puzzle.count('B') == num_black
    assert len(puzzle.split('\n')) == height
    assert all(len(row) == width for row in puzzle.split('\n'))


def test_is_solvable():
    """
    Verifies that the is_solvable function works correctly by testing it
    on a known solvable puzzle and an unsolvable puzzle
    :return: bool
    """
    solvable_puzzle = (
        ".W....\n"
        "...B..\n"
        "W.W...\n"
        ".....W\n"
        "..B...\n"
        ".....B\n"
    )
    assert is_solvable(solvable_puzzle)

    unsolvable_puzzle = (
        "WBBW..\n"
        "......\n"
        "......\n"
        "......\n"
        "......\n"
        "......\n"
    )
    assert not is_solvable(unsolvable_puzzle)


def test_generate_and_test():
    """
    Ensures that the generate_and_test function produces a puzzle with the correct dimensions
    and the specified number of circles, and that the puzzle is solvable
    :return: bool
    """
    width, height = 6, 6
    num_white, num_black = 3, 3
    solvable_puzzle = generate_and_test(width, height, num_white, num_black)
    assert solvable_puzzle.count('W') == num_white
    assert solvable_puzzle.count('B') == num_black
    assert len(solvable_puzzle.split('\n')) == height
    assert all(len(row) == width for row in solvable_puzzle.split('\n'))
    assert is_solvable(solvable_puzzle)


if __name__ == '__main__':
    pytest.main()
