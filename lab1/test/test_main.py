"""
Модуль для тестування HTTP запитів.
"""
# coverage run -m pytest test_main.py test_task31.py test_db_service.py test_problem_generator.py
# coverage run -m pytest test/.
# coverage report
import pytest
from starlette.testclient import TestClient
from fastapi import HTTPException
from main import app, validate_board_string, generate_correct_problem, solve_masyu

client = TestClient(app)


@pytest.mark.parametrize("board_str, expected", [
    (".W....\n...B..\nW.W...\n.....W\n..B...\n.....B", True),
    (".....B\n..B...\n....WW\n......\n...B..\nB...W.", True),
    ("....W.\nW.....\n.....B\nB.....\n.....W\n......", True),
    ("....W.\nW.....\n.B....\n..B..W\n......\n.W...B", True),
    (".W..WB\n......\n.W.W..\n....B.\n.....W\n..W...", True),
    (".W....\n", False),  # Incorrect format: <2 lines
    (".W....\nW.W...\n....W\n..B...\n.....B", False),  # Incorrect format: Lines not equal length
    (".W....\n...B..\nW.W...\n...W\n..B...\n.....B", False),  # Incorrect format: Lines not equal
    (".W....\n...B..\nW.W...\n...W\n..O...\n.....B", False),  # Incorrect format
    (".\n.\nW\n.\nB\n", False),  # Incorrect format: <2 lines
])
def test_validate_board_string(board_str, expected):
    """
    Function to test input string validation.

    Args:
        board_str, expected

    Returns:
        bool: correct or not.
    """
    assert validate_board_string(board_str) == expected


@pytest.mark.parametrize("board_str, expected_status_code", [
    (".W....\\n...B..\\nW.W...\\n.....W\\n..B...\\n.....B", 200),  # Correct input
    (".W....\\n...B..\\nW.W...\\n.....W\\n..B...\\n.....", 400),  # Incorrect input: Lines not equal
    (".W....\\n", 400),  # Incorrect input: <2 lines
])
def test_get_solution(board_str, expected_status_code):
    """
    Function to test HTTP solution response.

    Args:
        board_str, expected_status_code

    Returns:
        bool: correct or not.
    """
    response = client.post("/solve?problem_str=" + board_str)
    assert response.status_code == expected_status_code


def test_generate():
    """
    Function to test HTTP conditions generation.

    Args:

    Returns:
        bool: correct or not.
    """
    response = client.get("/generate")
    assert response.status_code == 200
    assert "problem" in response.json()


def test_generate_correct_problem():
    """
    Function to test generation.

    Args:

    Returns:
        bool: correct or not.
    """
    # Викликаємо функцію для отримання умови проблеми
    problem = generate_correct_problem()
    # Перевіряємо, чи повертається рядок
    assert isinstance(problem, str)
    # Перевіряємо, чи повернений рядок містить правильний формат
    assert validate_board_string(problem), "Returned string doesn't match the expected format"


def test_solve_masyu():
    """
    Function to test solvong masyu.

    Args:

    Returns:
        bool: correct or not.
    """
    # Передаємо правильний рядок для розв'язання
    correct_problem = ".W....\n...B..\nW.W...\n.....W\n..B...\n.....B"
    solution = solve_masyu(correct_problem)
    # Перевіряємо, чи отримано правильне рішення
    assert solution is not None, "Solution should not be None"

    wrong_problem = ".W....\n...B..\nW.W...\n.....W\n..B...\n..B..B"
    with pytest.raises(HTTPException):
        solve_masyu(wrong_problem)


def test_getproblem():
    """
    Function to test getting problem.
    Args:
    Returns:
        bool: correct or not.
    """
    response = client.get("/getproblem?problem_id=1")
    assert response.status_code == 200
    assert "problem" in response.json()


def test_add_problem():
    """
    Function to test adding problem.
    Args:
    Returns:
        bool: correct or not.
    """
    problem_str = ".W....\\n...B..\\nW.W...\\n.....W\\n..B...\\n.....B"

    response = client.post("/addproblem?problem_str="+problem_str)

    assert response.status_code == 200
    assert "problem" in response.json()


def test_get_solvings_by_problem_id():
    """
    Function to test getting solvings by id.
    Args:
    Returns:
        bool: correct or not.
    """
    response = client.get("/get-solvings-by-id?problem_id=1")
    assert response.status_code == 200
    assert "solvings" in response.json()


def test_get_solvings_by_problem():
    """
    Function to test getting solvings by problem.
    Args:
    Returns:
        bool: correct or not.
    """
    problem_str = ".W....\\n...B..\\nW.W...\\n.....W\\n..B...\\n.....B"
    response = client.get("/get-solvings-by-problem?problem_str=" + problem_str)
    assert response.status_code == 200
    assert "solvings" in response.json()


def test_add_solving_by_problem_id(mocker):
    """
    Function to test adding solvings.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    # Мокуємо функцію вставки розв'язку
    mocker.patch('main.db.insert_solving_to_existing_problem_id').return_value = True

    response = client.post("/add-solving-by-id?problem_id=22&solving_str=solving")

    assert response.status_code == 200
    assert "problem_id" in response.json()
    assert "solving" in response.json()
    assert response.json()["problem_id"] == 22
    assert response.json()["solving"] == "solving"


def test_add_solving_by_problem_id_error(mocker):
    """
    Function to test adding solvings error.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    # Мокуємо функцію вставки розв'язку
    mocker.patch('main.db.insert_solving_to_existing_problem_id').return_value = False

    response = client.post("/add-solving-by-id?problem_id=22&solving_str=solving")

    assert response.status_code == 400
    assert "detail" in response.json()
    assert response.json()["detail"] == "Помилка додавання нової умови"


def test_add_solving_by_problem(mocker):
    """
    Function to test adding solvings.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    # Мокуємо функцію вставки розв'язку
    mocker.patch('main.db.insert_solving_to_existing_problem').return_value = True
    problem_str = ".W....\\n...B..\\nW.W...\\n.....W\\n..B...\\n.....B"

    response = client.post("/add-solving-by-problem?problem="
                           + problem_str + "&solving_str=solving")

    assert response.status_code == 200
    assert "problem_str" in response.json()
    assert "solving" in response.json()
    assert response.json()["problem_str"] == problem_str
    assert response.json()["solving"] == "solving"


def test_add_solving_by_problem_error(mocker):
    """
    Function to test adding solvings error.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    # Мокуємо функцію вставки розв'язку
    mocker.patch('main.db.insert_solving_to_existing_problem').return_value = False

    response = client.post("/add-solving-by-problem?problem=22&solving_str=solving")

    assert response.status_code == 400
    assert "detail" in response.json()
    assert response.json()["detail"] == "Помилка додавання нової умови"


def test_register(mocker):
    """
    Function to test registration.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    mocker.patch('main.db.add_user').return_value = True

    response = client.post("/register?username=name&password_str=pass")

    assert response.status_code == 200
    assert "message" in response.json()
    assert "developer_key" in response.json()
    assert response.json()["message"] == "Реєстрація успішна"


def test_register_error(mocker):
    """
    Function to test registration error.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    mocker.patch('main.db.add_user').return_value = False

    response = client.post("/register?username=name&password_str=pass")

    assert response.status_code == 400
    assert "detail" in response.json()
    assert response.json()["detail"] == "Помилка реєстрації"


def test_register_user_exists(mocker):
    """
    Function to test if user exists.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    mocker.patch('main.db.check_user_exists').return_value = True

    response = client.post("/register?username=name&password_str=pass")

    assert response.status_code == 400
    assert "detail" in response.json()
    assert response.json()["detail"] == "Користувач з таким ім'ям вже існує"


def test_login_by_key(mocker):
    """
    Function to test key login.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    mocker.patch('main.authenticate_user_by_key').return_value = True

    response = client.post("/login?username=name&password_str=pass&developer_key=key")

    assert response.status_code == 200
    assert "message" in response.json()
    assert response.json()["message"] == "Успішний вхід за допомогою девелоперського ключа"


def test_login_by_password(mocker):
    """
    Function to test password login.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    mocker.patch('main.authenticate_user_by_password').return_value = True

    response = client.post("/login?username=name&password_str=pass&developer_key=key")

    assert response.status_code == 200
    assert "message" in response.json()
    assert response.json()["message"] == "Успішний вхід за допомогою пароля"


def test_login_error(mocker):
    """
    Function to test login error.
    Args:
        mocker
    Returns:
        bool: correct or not.
    """
    mocker.patch('main.authenticate_user_by_password').return_value = False
    mocker.patch('main.authenticate_user_by_key').return_value = False

    response = client.post("/login?username=name&password_str=pass&developer_key=key")

    assert response.status_code == 401
    assert "detail" in response.json()
    assert response.json()["detail"] == "Неправильне ім'я користувача або пароль або ключ"
